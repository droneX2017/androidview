
var ros;
var ip = location.host;
var auth=false;

if (location.protocol=='https:'){
  restProto = 'https://';
  wsProto = 'wss://';
  auth=true;

}else{
  restProto = 'http://';
  wsProto = 'ws://';
  
}
restPath = restProto + ip;
wsPath = wsProto + ip;

// (function() {

        if (!sessionStorage.length) {
          // Ask other tabs for session storage
          localStorage.setItem('getSessionStorage', Date.now());
        };

        window.addEventListener('storage', function(event) {

          // console.log('storage event', event);

          if (event.key == 'getSessionStorage') {
            // Some tab asked for the sessionStorage -> send it

            localStorage.setItem('sessionStorage', JSON.stringify(sessionStorage));
            localStorage.removeItem('sessionStorage');

          } else if (event.key == 'sessionStorage' && !sessionStorage.length) {
            // sessionStorage is empty -> fill it

            var data = JSON.parse(event.newValue);

            for (key in data) {
              sessionStorage.setItem(key, data[key]);
            }

            // showSessionStorage();
          }
        });

        window.onbeforeunload = function() {
          //sessionStorage.clear();
        };


      // })();

if(auth)
{
 setTimeout(function(){
      var msgdata = {};
      msgdata['username'] = sessionStorage.getItem('username');
      // console.log('tokkn: ',sessionStorage.getItem('token'));

      $.ajax({
        type: "POST",
        headers: {'Authentication-Token': sessionStorage.getItem('token')},
        dataType: "json",
        contentType: "application/json",
        data: JSON.stringify(msgdata),
        url: restPath+"/getname",
        
        success: function(data){

          // console.log(data);
          //$('#user_name').html("<b>&nbsp;&nbsp;"+data.username+"</b>");

        },
        error: function(data){
          console.log("error in get user name");
          console.log(data);
          if(data.status == '401')
            window.location.href = restPath+"/logout?next=login";
        }

      });

 },500);
}


function rosInitialize(){
    ros = new ROSLIB.Ros({
      url : wsPath +'/websocket'
    });


    ros.on('connection', function() {
      console.log('Connected to websocket server.');
      socketCallback();
    });

    ros.on('error', function(error) {
      console.log('Error connecting to websocket server: ', error);
    });

    ros.on('close', function() {
      console.log('Connection to websocket server closed.');
    });

    if(auth){
	    var rauth = new ROSLIB.Message({
		                 "op": "auth",
		                 "mac" : sessionStorage.getItem('token'),

		             });

	    ros.authenticate(rauth);
    }
    // setTimeout(function(){},3000);
}

