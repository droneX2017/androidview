package com.example.jagdish.remoteview;

import android.animation.Animator;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.wang.avi.AVLoadingIndicatorView;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.HttpHostConnectException;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.Locale;

public class PathFlow extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    SupportMapFragment mapFragment;

    private View mDecorView;
    WebView web;

    AVLoadingIndicatorView loader;
    HttpClient httpClient;
    SharedPreferences remoteSetting;

    int frame_waypoint, command_waypoint, wp_transfered, wp_received, countofWayPoint, zoom_in_level;
    Boolean recordWayPoint, is_current_waypoint, autocontinue_waypoint, captureLiveDroneCoordinate, setFocusable, isWayPointSet;
    float param1_waypoint, param2_waypoint, param3_waypoint, param4_waypoint, z_alt_waypoint, radius;
    Double droneLat, droneLng;
    double total_distance_current_waypoint, distance[];

    String url, dialogMessage, urlvideo, userEnterUrl, global_pos, setWaypointUrl, getWaypointUrl, clearWayPoint, executeWayPoint, pauseExecuteWayPoint;
    String wayPointString;

    Bitmap droneMapIcon;

    ImageButton tracking, clear, wayPoint, getStoreWayPoint, clearWayPointButton, executeWaypointButton;
    private CardView circularMap, circularVideo;
    private CardView.LayoutParams largeCadLayout, smallCadLayout;
    private TextView gpsStatus, satellite, totalDistance;
    FrameLayout frame, mapvideoframe;
    RoundedCornerLayout roundedCornerLayout;

    PolylineOptions wayPointPolyline, liveWayPointPolyline, receiveWayPointPolyline;
    Polyline wayPointPath, liveWayPointPath, receiveWayPointPath;

    List  wayPointLatLngArray, liveWayPointLatLngArray, finalArray;

    Handler globalPosHandler;
    Runnable globalPosRunnable;

    Marker droneCurLocation, dronePreLocation;
    Location preLocation, curLocation;
    LatLng curLatLng;
    JSONObject wayPointJSONObject;
    JSONArray receiveWayPointJSONArray;
    Vibrator vibrator;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_path_flow);


        //------------------------------------------------------------------Screen Rendering section
        loader = (AVLoadingIndicatorView) findViewById(R.id.laoder);
        //------------------------------------------------------------Screen Rendering section ended

        //make full transparent statusBar
        if (Build.VERSION.SDK_INT >= 19 && Build.VERSION.SDK_INT < 21) {
            setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, true);
            setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION,true);
        }
        if (Build.VERSION.SDK_INT >= 21) {
            setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, true);
            setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION,true);
        }

        //--------------------------------------------------------------------initialization section
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        frame_waypoint = 3;
        command_waypoint = 16;
        zoom_in_level = 19;

        is_current_waypoint = false;
        autocontinue_waypoint = true;
        captureLiveDroneCoordinate = false;
        setFocusable = true;
        isWayPointSet =false;
        recordWayPoint = false;
        isWayPointSet = false;

        param1_waypoint = (float) 1.0;
        param2_waypoint = (float) 8.0;
        param3_waypoint = (float) 0.0;
        param4_waypoint = (float) 0.0;
        z_alt_waypoint = (float) 1.0;
        total_distance_current_waypoint = 0.0;

        url = getResources().getString(R.string.ip);
        global_pos = ""+getResources().getString(R.string.mavros_server_service)+""+getResources().getString(R.string.global_pos);
        setWaypointUrl = ""+getResources().getString(R.string.ros_server_service)+""+getResources().getString(R.string.set_waypoint);
        getWaypointUrl = ""+getResources().getString(R.string.ros_server_service)+""+getResources().getString(R.string.get_waypoint);
        clearWayPoint = ""+getResources().getString(R.string.ros_server_service)+""+getResources().getString(R.string.clear_waypoint);
        pauseExecuteWayPoint = ""+getResources().getString(R.string.ros_server_service)+""+getResources().getString(R.string.pause_waypoint);
        executeWayPoint = ""+getResources().getString(R.string.ros_server_service)+""+getResources().getString(R.string.execute_waypoint);

        remoteSetting = getApplicationContext().getSharedPreferences("RemoteSetting",MODE_PRIVATE);
        userEnterUrl = "http://"+remoteSetting.getString("ip","").trim();
        if(url.equals(userEnterUrl)) {
            urlvideo = ""+url+""+getResources().getString(R.string.port)+""+getResources().getString(R.string.videostream);
        } else {
            urlvideo = ""+userEnterUrl+""+getResources().getString(R.string.port)+""+getResources().getString(R.string.videostream);
            url = userEnterUrl;
        }

        httpClient = new DefaultHttpClient();
        preLocation = new Location("");
        curLocation = new Location("");
        globalPosHandler = new Handler();

        gpsStatus = (TextView) findViewById(R.id.gpsStatus);
        satellite = (TextView) findViewById(R.id.satellite);
        totalDistance = (TextView) findViewById(R.id.totaldistancecurwaypoint);

        tracking = (ImageButton) findViewById(R.id.startTracking);
        clear = (ImageButton) findViewById(R.id.clearMap);
        wayPoint = (ImageButton) findViewById(R.id.waypointbutton);
        clearWayPointButton = (ImageButton) findViewById(R.id.clearwaypoint);
        executeWaypointButton = (ImageButton) findViewById(R.id.executepausewaypoint);
        getStoreWayPoint = (ImageButton) findViewById(R.id.getstorewaypointbutton);

        circularMap = (CardView) findViewById(R.id.circularmap);
        circularVideo = (CardView) findViewById(R.id.circularvideo);
        smallCadLayout = (CardView.LayoutParams) circularVideo.getLayoutParams();
        largeCadLayout = (CardView.LayoutParams) circularMap.getLayoutParams();

        frame = (FrameLayout)findViewById(R.id.frame1);
        mapvideoframe = (FrameLayout)findViewById(R.id.mapvideoframe);
        roundedCornerLayout = (RoundedCornerLayout) findViewById(R.id.rounded_layout);
        web = new WebView(getApplicationContext());
        vibrator = (Vibrator)this.getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);

        droneMapIcon = Bitmap.createScaledBitmap(((BitmapDrawable)getResources().getDrawable(R.mipmap.airscrew)).getBitmap(),70,70,false);
        circularVideo.setPreventCornerOverlap(false);
        radius = circularVideo.getRadius();
        circularVideo.bringToFront();
        totalDistance.setText("Dis: "+total_distance_current_waypoint+" m");
        //--------------------------------------------------------------initialization section ended

        //===========================// Video Streaming area //===================================//
        String video = "<body style=\"margin:0px; padding:0px; width:100%; height:100%;\">" +
                "<div  style=\" width:inherit; height:inherit;\"> " +
                "<img  style=\"-webkit-user-select: none; width:inherit; height:inherit;\"  src=\""+urlvideo+"\"/>"+
                "</div>"+
                "</body>";

        web.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        web.setBackgroundColor(Color.TRANSPARENT);
        web.loadData(video,"text/html","UTF-8");
        frame.addView(web);

        if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.LOLLIPOP) {
            frame.removeAllViews();
            circularVideo.removeAllViews();
            mapvideoframe.removeView(circularVideo);
            circularMap.bringToFront();

            roundedCornerLayout.addView(web);
            roundedCornerLayout.bringToFront();
            Log.e("Rounded","sjngjsdnkglndslk");
        }
        //===========================//End Video Streaming area //================================//

        //-------------------------------------------------------------------Button listener started
        //Clear Button listener
        if(clear != null) {
            clear.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    circularReveal(v);
                    vibrator.vibrate(100);
                    if (mMap != null) {
                        mMap.clear();
                    }
                    Toast.makeText(getApplicationContext(),"Clearing map.",Toast.LENGTH_SHORT).show();
                    wayPointLatLngArray = new ArrayList();
                    liveWayPointLatLngArray = new ArrayList();
                    liveWayPointPath  = null;
                    liveWayPointPolyline = null;
                }
            });
        } /*clear != null*/

        if(clearWayPointButton != null) {
            clearWayPointButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    circularReveal(v);
                    vibrator.vibrate(100);
                    //Toast.makeText(getApplicationContext(),"Clearing waypoint from flytpod",Toast.LENGTH_SHORT).show();
                    new EPCWayPoint().execute(url+clearWayPoint,"Clearing waypoint");
                }
            });
        }

        if(executeWaypointButton != null) {
            executeWaypointButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    circularReveal(v);
                    vibrator.vibrate(100);
                    if(isWayPointSet) {
                        if (executeWaypointButton.getTag().toString().equals("start")) {
                            //Toast.makeText(getApplicationContext(),"Execute waypoint",Toast.LENGTH_SHORT).show();
                            new EPCWayPoint().execute(url + executeWayPoint, "Waypoint execute");
                        } else if (executeWaypointButton.getTag().toString().equals("stop")) {
                            //Toast.makeText(getApplicationContext(),"Pause waypoint",Toast.LENGTH_SHORT).show();
                            new EPCWayPoint().execute(url + pauseExecuteWayPoint, "Waypoint execution pause");
                        }
                    } else {
                        Toast.makeText(getApplicationContext(),"Please set waypoint.",Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }

        if(getStoreWayPoint != null) {
            getStoreWayPoint.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    circularReveal(v);
                    vibrator.vibrate(100);
                    //Toast.makeText(getApplicationContext(),"Getting waypoint from flytpod.",Toast.LENGTH_SHORT).show();
                    new getWayPoint().execute();
                }
            });
        }

        if(wayPoint != null) {
            wayPoint.setOnClickListener(new View.OnClickListener() {
                String tag;

                @Override
                public void onClick(View v) {
                    circularReveal(v);
                    vibrator.vibrate(100);
                    tag = wayPoint.getTag().toString();
                    if (tag.equals("start")) {
                        wayPointLatLngArray = new ArrayList();
                        setFocusable = false;
                        countofWayPoint = 0;
                        recordWayPoint = true;

                        wayPoint.setImageResource(R.mipmap.videostop);
                        wayPoint.setTag("stop");
                    } else if (tag.equals("stop")) {
                        recordWayPoint = false;
                        setFocusable = true;

                        createWayPointPath();
                        createWayPointJSONObject(wayPointLatLngArray);
                        wayPoint.setImageResource(R.mipmap.waypoint);
                        wayPoint.setTag("start");
                    }
                }
            });
        }

        /*Start tracking button listener
        * To stop tracking*/
        if(tracking != null) {
            tracking.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    circularReveal(v);
                    vibrator.vibrate(100);
                    if(droneLat == null || droneLng == null) {
                        Toast.makeText(getApplicationContext(),"Location is not available.\nPlease check flytpod controller.",Toast.LENGTH_SHORT).show();
                        gpsStatus.setText("No Position.");
                    } else {
                        if(tracking.getTag().toString().equals("start")) {
                            Toast.makeText(getApplicationContext(), "Starting Tracking", Toast.LENGTH_SHORT).show();
                            liveWayPointLatLngArray = new ArrayList();
                            tracking.setImageResource(R.mipmap.videostop);
                            tracking.setTag("stop");

                            captureLiveDroneCoordinate = true;
                        } else if(tracking.getTag().toString().equals("stop")) {
                            tracking.setImageResource(R.mipmap.start_tracking);
                            tracking.setTag("start");

                            captureLiveDroneCoordinate = false;
                            Toast.makeText(getApplicationContext(), "Tracking stopped", Toast.LENGTH_SHORT).show();
                            createLiveDroneRecordedPath();
                        }
                    }
                }
            });
        } /*startTracking != null*/
        //---------------------------------------------------------------------Button listener ended

        //handling auto hide of status bar and navigation bar
        mDecorView = getWindow().getDecorView();
        mDecorView.setOnSystemUiVisibilityChangeListener
                ( new View.OnSystemUiVisibilityChangeListener() {
                    @Override
                    public void onSystemUiVisibilityChange(int visibility) {
                        // Note that system bars will only be "visible" if none of the
                        // LOW_PROFILE, HIDE_NAVIGATION, or FULLSCREEN flags are set.
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                hideSystemUI();
                            }
                        }, 1500);
                    }
                });
        mDecorView.setKeepScreenOn(true);
        hideSystemUI();

        globalPosRunnable = new Runnable() {
            @Override
            public void run() {
                new getGlobalPosition().execute();
                globalPosHandler.postDelayed(this,1500);
            }
        };
        globalPosHandler.postDelayed(globalPosRunnable,1000);
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        //mMap.getUiSettings().setCompassEnabled(false);
        mMap.setMyLocationEnabled(false);
        mMap.getUiSettings().setMyLocationButtonEnabled(false);

            mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                if(recordWayPoint) {
                    if(countofWayPoint >= 11) {
                        Toast.makeText(getApplicationContext(),"Max limit of waypoint reached.",Toast.LENGTH_SHORT).show();
                        recordWayPoint = false;
                        finalArray = wayPointLatLngArray;
                        wayPoint.setImageResource(R.mipmap.waypoint);
                        wayPoint.setTag("start");
                        dialogMessage = "Max limit of waypoint reached.\nDo you want to store current path?";
                        askToStoreRecordedWaypoint();
                        return;
                    }
                    wayPointLatLngArray.add(latLng);
                    double lat, lng;
                    lng = latLng.longitude;
                    lat = latLng.latitude;
                    countofWayPoint++;
                    mMap.addMarker(new MarkerOptions().position( latLng).title("waypoint:").snippet("lat: "+lat+"\nlng: "+lng)).setAlpha((float) 0.3);
                } else {
                    if (circularMap != null) {
                        if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.LOLLIPOP) {
                            /*if(circularMap.getTag().toString().equals("small")) {
                                //Map view
                                circularMap.setTag("large");
                                mapvideoframe.removeView(web);
                                mapvideoframe.addView(circularMap);

                                //video view
                                roundedCornerLayout.addView(web);
                                roundedCornerLayout.bringToFront();
                            } else if(circularMap.getTag().toString().equals("large")) {
                                //map view
                                circularMap.setTag("small");
                                roundedCornerLayout.removeAllViews();
                                roundedCornerLayout.addView(circularMap);

                                //video view

                            }*/
                        } else {
                            if (circularMap.getTag().toString().equals("small")) {
                                //Map View
                                circularMap.setLayoutParams(largeCadLayout);
                                circularMap.setRadius(0);
                                circularMap.setTag("large");

                                //video View
                                circularVideo.setLayoutParams(smallCadLayout);
                                circularVideo.setRadius(radius);
                                circularVideo.setTag("small");
                                circularVideo.bringToFront();
                            } else if (circularMap.getTag().toString().equals("large")) {
                                //Map view
                                circularMap.setLayoutParams(smallCadLayout);
                                circularMap.setRadius(radius);
                                circularMap.setTag("small");
                                circularMap.bringToFront();

                                //Video view
                                circularVideo.setLayoutParams(largeCadLayout);
                                circularVideo.setRadius(0);
                                circularVideo.setTag("large");
                            }
                        }
                    }
                }
            }
        });
        loader.hide();
    }

    public void circularReveal(View v) {
        Animator animatorCircularReveal = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            animatorCircularReveal = ViewAnimationUtils.createCircularReveal(
                    v,
                    0,
                    0,
                    0,
                    (float) Math.hypot(v.getWidth(), v.getHeight()));

            animatorCircularReveal.setInterpolator(new AccelerateDecelerateInterpolator());
            animatorCircularReveal.start();
        }
    }

    public void createWayPointJSONObject(List latlngList) {
        int i = 0;
        LatLng latlngtemp;
        Location preLo, curLo;
        preLo = new Location("");

        if(latlngList == null) {
            Toast.makeText(getApplicationContext(),"Sorry something is wrong.",Toast.LENGTH_SHORT).show();
            return;
        }
        if(latlngList.isEmpty()) {
            Toast.makeText(getApplicationContext(),"Path is not recorded",Toast.LENGTH_SHORT).show();
            return;
        }
        if(latlngList.size() > 11) {
            Toast.makeText(getApplicationContext(),"Limit exhausted of waypoint\nPlease reduce number of waypoint to 11",Toast.LENGTH_SHORT).show();
            return;
        }

        wayPointString = "";
        total_distance_current_waypoint = 0.0;
        distance = new double[latlngList.size()];
        ListIterator latlngIterator = latlngList.listIterator();
        while(latlngIterator.hasNext()) {
            latlngtemp = (LatLng) latlngIterator.next();
            curLo = new Location("");
            curLo.setLatitude(latlngtemp.latitude);
            curLo.setLongitude(latlngtemp.longitude);

            if(latlngIterator.nextIndex() == 1) {
                wayPointString += "{" +
                        "frame : "+frame_waypoint+
                        ", command : "+command_waypoint+
                        ", is_current : true"+
                        ", autocontinue : "+autocontinue_waypoint+
                        ", param1 : "+param1_waypoint+
                        ", param2 : "+param2_waypoint+
                        ", param3 : "+param3_waypoint+
                        ", param4 : "+param4_waypoint+
                        ", x_lat : "+latlngtemp.latitude+
                        ", y_long : "+latlngtemp.longitude+
                        ", z_alt : "+z_alt_waypoint+
                        "},";
                preLo = curLo;
            } else if(!latlngIterator.hasNext()) {
                wayPointString += "{" +
                        "frame : "+frame_waypoint+
                        ", command : "+command_waypoint+
                        ", is_current : "+is_current_waypoint+
                        ", autocontinue : "+autocontinue_waypoint+
                        ", param1 : "+param1_waypoint+
                        ", param2 : "+param2_waypoint+
                        ", param3 : "+param3_waypoint+
                        ", param4 : "+param4_waypoint+
                        ", x_lat : "+latlngtemp.latitude+
                        ", y_long : "+latlngtemp.longitude+
                        ", z_alt : "+z_alt_waypoint+
                        "}";
                distance[i] = preLo.distanceTo(curLo);
                total_distance_current_waypoint += distance[i];
                preLo = curLo;
            } else {
                wayPointString += "{" +
                        "frame : "+frame_waypoint+
                        ", command : "+command_waypoint+
                        ", is_current : "+is_current_waypoint+
                        ", autocontinue : "+autocontinue_waypoint+
                        ", param1 : "+param1_waypoint+
                        ", param2 : "+param2_waypoint+
                        ", param3 : "+param3_waypoint+
                        ", param4 : "+param4_waypoint+
                        ", x_lat : "+latlngtemp.latitude+
                        ", y_long : "+latlngtemp.longitude+
                        ", z_alt : "+z_alt_waypoint+
                        "},";
                distance[i] = preLo.distanceTo(curLo);
                total_distance_current_waypoint += distance[i];
                preLo = curLo;
            }
        } /*latlngIterator.hasNext()*/

        wayPointString = "{ waypoints : ["+wayPointString+"] }";

        try {
            wayPointJSONObject = new JSONObject(wayPointString);
            new setWayPoint().execute();
        } catch (JSONException e) {
            Log.e("waypointstring ","error in string");
        }
    }

    public void createLiveDroneRecordedPath() {
        if(liveWayPointLatLngArray == null) {
            Toast.makeText(getApplicationContext(),"Sorry something is wrong.",Toast.LENGTH_SHORT).show();
            return;
        }
        if(liveWayPointLatLngArray.isEmpty()) {
            Toast.makeText(getApplicationContext(),"Path is not recorded",Toast.LENGTH_SHORT).show();
            return;
        }

        ListIterator lla = liveWayPointLatLngArray.listIterator();
        while(lla.hasNext()) {
            mMap.addMarker(new MarkerOptions().position((LatLng) lla.next()).title("Waypoint: "+lla.nextIndex()+1)).setAlpha((float) 0.3);
        }

        liveWayPointPolyline = new PolylineOptions()
                .addAll((Iterable<LatLng>) liveWayPointLatLngArray)
                .width(5)
                .color(Color.GREEN);
        liveWayPointPath = mMap.addPolyline(liveWayPointPolyline);

        finalArray = liveWayPointLatLngArray;
        dialogMessage = "Do you want to store current path?";
        askToStoreRecordedWaypoint();
    }

    public void createPathfromReceiveWayPoint() {
        JSONObject temp;
        Location preLo, curLo;
        preLo = new Location("");
        Double lat, lng, alt;
        LatLng tempLatLng;
        List receiveWayPointLatLngArray = new ArrayList();

        if(receiveWayPointJSONArray == null) {
            Toast.makeText(getApplicationContext(),"Sorry something is wrong.",Toast.LENGTH_SHORT).show();
            return;
        }

        int len = receiveWayPointJSONArray.length();
        total_distance_current_waypoint = 0.0;
        distance = new double[wp_received];

        for(int i=0 ; i<len ; i++) {
            curLo = new Location("");
            try {
                temp = receiveWayPointJSONArray.getJSONObject(i);

                lat = temp.getDouble("x_lat");
                lng = temp.getDouble("y_long");
                alt = temp.getDouble("z_alt");

                curLo.setLatitude(lat);
                curLo.setLongitude(lng);
                tempLatLng = new LatLng(lat,lng);
                receiveWayPointLatLngArray.add(tempLatLng);

                mMap.addMarker(new MarkerOptions().position(tempLatLng).title("Waypoint: "+(i+1)).snippet("Relative alt: "+alt).alpha((float)0.5)).setAlpha((float) 0.3);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if(i == 0) {
                preLo = curLo;
            } else {
                distance[i] = preLo.distanceTo(curLo);
                total_distance_current_waypoint += distance[i];
            }
        }

        if(total_distance_current_waypoint == 0) {
            isWayPointSet = false;
        } else {
            isWayPointSet = true;
        }
        totalDistance.setText(String.format(Locale.ENGLISH,"Dis: %.3f m",total_distance_current_waypoint));

        receiveWayPointPolyline = new PolylineOptions()
                .addAll((Iterable<LatLng>) receiveWayPointLatLngArray)
                .width(5)
                .color(Color.CYAN);
        receiveWayPointPath = mMap.addPolyline(receiveWayPointPolyline);
    }

    public void setDronePositionOnMap() {
        if(dronePreLocation == null) {
            if(droneLat == null || droneLng == null) {
                gpsStatus.setText("No Position.");
            } else {
                curLatLng = new LatLng(droneLat,droneLng);
                if(captureLiveDroneCoordinate) {
                    liveWayPointLatLngArray.add(curLatLng);
                }
                droneCurLocation = mMap.addMarker(new MarkerOptions()
                    .position(curLatLng)
                    .title("Drone")
                    .flat(true)
                    .snippet("Lat: "+droneLat+"\nLng: "+droneLng)
                    .icon(BitmapDescriptorFactory.fromBitmap(droneMapIcon)));

                droneCurLocation.setAlpha((float)0.5);
                if(setFocusable) {
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(curLatLng, zoom_in_level));
                }
                dronePreLocation = droneCurLocation;
                preLocation.setLatitude(droneLat);
                preLocation.setLongitude(droneLng);
            }
        } else {
            if(droneLat == null || droneLng == null) {
                Toast.makeText(getApplicationContext(),"No Position.",Toast.LENGTH_SHORT).show();
            } else {
                dronePreLocation.remove();
                curLocation.setLatitude(droneLat);
                curLocation.setLongitude(droneLng);

                curLatLng = new LatLng(droneLat,droneLng);
                if(captureLiveDroneCoordinate) {
                    /*if(preLocation.distanceTo(curLocation) >= .5) {*/
                        liveWayPointLatLngArray.add(curLatLng);
                    /*}*/
                }
                droneCurLocation = mMap.addMarker(new MarkerOptions()
                        .position(curLatLng)
                        .title("Drone")
                        .flat(true)
                        .snippet("Lat: "+droneLat+"\nLng: "+droneLng)
                        .icon(BitmapDescriptorFactory.fromBitmap(droneMapIcon)));

                droneCurLocation.setAlpha((float)0.5);
                if(setFocusable) {
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(curLatLng, zoom_in_level));
                }
                dronePreLocation = droneCurLocation;
                preLocation = curLocation;
            }
        }
    }

    public void createWayPointPath() {
        if(wayPointLatLngArray.isEmpty()) {
            Toast.makeText(getApplicationContext(),"No waypoint are marked.",Toast.LENGTH_SHORT).show();
            return;
        }
        if(wayPointLatLngArray == null) {
            Toast.makeText(getApplicationContext(),"Something went wrong.",Toast.LENGTH_SHORT).show();
            return;
        }
        wayPointPolyline = new PolylineOptions()
                .addAll((Iterable<LatLng>) wayPointLatLngArray)
                .width(5)
                .color(Color.BLUE);
        wayPointPath = mMap.addPolyline(wayPointPolyline);
    }

    class setWayPoint extends AsyncTask<String,String,String> {
        HttpResponse response;
        BufferedReader reader;
        String jsonResponse, success;
        JSONObject jsonObject;

        @Override
        protected void onPreExecute() {
            loader.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                Log.e("IP",url + setWaypointUrl);
                HttpPost httpPost = new HttpPost(url+setWaypointUrl);
                httpPost.setHeader("Content-type", "application/json");

                HttpParams httpParams = new BasicHttpParams();
                HttpClient httpclient = new DefaultHttpClient(httpParams);
                httpPost.setEntity(new ByteArrayEntity(
                        wayPointJSONObject.toString().getBytes("UTF8")));
                response = httpclient.execute(httpPost);

                reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(),"UTF-8"));
                jsonResponse = reader.readLine();

            } catch (NullPointerException e) {
                return "error";
            } catch (MalformedURLException e) {
                return "error";
            } catch (HttpHostConnectException e) {
                e.printStackTrace();
                return "failed";
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if(s != null) {
                if(s.equals("failed")) {
                    Toast.makeText(getApplicationContext(), "Connection refused!!!", Toast.LENGTH_SHORT).show();
                    loader.hide();
                    return;
                }
            }

            try{
                if(jsonResponse == null) {
                    throw(new NullPointerException());
                }
                jsonObject = new JSONObject(jsonResponse);
                success = jsonObject.getString("success");
                if(success.equals("true")) {
                    wp_transfered = jsonObject.getInt("wp_transfered");
                    Toast.makeText(getApplicationContext(),"Waypoint set in drone.",Toast.LENGTH_SHORT).show();
                    if(total_distance_current_waypoint == 0) {
                        isWayPointSet = false;
                    } else {
                        isWayPointSet = true;
                    }
                    totalDistance.setText(String.format(Locale.ENGLISH,"Dis: %.3f m",total_distance_current_waypoint));
                } else {
                    Toast.makeText(getApplicationContext(),"Waypoint doesn't set.",Toast.LENGTH_SHORT).show();
                }
            }
            catch (NullPointerException e){
                Toast.makeText(getApplicationContext(),"Null pointer",Toast.LENGTH_SHORT).show();
            } catch (JSONException e) {
                Toast.makeText(getApplicationContext(),"Command Rejected.",Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
            loader.hide();
        }
    }

    class getWayPoint extends AsyncTask<String,String,String> {
        HttpResponse response;
        BufferedReader reader;
        String jsonResponse, success;
        JSONObject jsonObject;

        @Override
        protected void onPreExecute() {
            loader.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                Log.e("IP",url + getWaypointUrl);
                response = httpClient.execute(new HttpGet(url + getWaypointUrl));

                reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(),"UTF-8"));
                jsonResponse = reader.readLine();

            } catch (NullPointerException e) {
                return "error";
            } catch (MalformedURLException e) {
                return "error";
            } catch (HttpHostConnectException e) {
                e.printStackTrace();
                return "failed";
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if(s != null) {
                if(s.equals("failed")) {
                    Toast.makeText(getApplicationContext(), "Connection refused!!!", Toast.LENGTH_SHORT).show();
                    loader.hide();
                    return;
                }
            }

            try{
                if(jsonResponse == null) {
                    throw(new NullPointerException());
                }
                jsonObject = new JSONObject(jsonResponse);

                success = jsonObject.getString("success");
                if(success.equals("true")) {
                    wp_received = jsonObject.getInt("wp_received");
                    if(wp_received == 0) {
                        Toast.makeText(getApplicationContext(),"0 waypoint.",Toast.LENGTH_SHORT).show();
                        isWayPointSet = false;
                        totalDistance.setText("Dis: 0 m");
                    } else {
                        Toast.makeText(getApplicationContext(),wp_received+" waypoint.",Toast.LENGTH_SHORT).show();
                        receiveWayPointJSONArray = jsonObject.getJSONArray("waypoints");
                        createPathfromReceiveWayPoint();
                    }
                } else {
                    Toast.makeText(getApplicationContext(),"Waypoint isn't set.",Toast.LENGTH_SHORT).show();
                }
            }
            catch (NullPointerException e){
                Toast.makeText(getApplicationContext(),"Null pointer",Toast.LENGTH_SHORT).show();
            } catch (JSONException e) {
                Toast.makeText(getApplicationContext(),"Command Rejected.",Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
            loader.hide();
        }
    }

    class EPCWayPoint extends AsyncTask<String,String,String> {
        HttpResponse response;
        BufferedReader reader;
        String jsonResponse, success;
        JSONObject jsonObject;

        @Override
        protected void onPreExecute() {
            loader.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                Log.e("IP",strings[0]);
                response = httpClient.execute(new HttpGet(strings[0]));

                reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(),"UTF-8"));
                jsonResponse = reader.readLine();
                return strings[1];

            } catch (NullPointerException e) {
                return "error";
            } catch (MalformedURLException e) {
                return "error";
            } catch (HttpHostConnectException e) {
                e.printStackTrace();
                return "failed";
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if(s != null) {
                if(s.equals("failed")) {
                    Toast.makeText(getApplicationContext(), "Connection refused!!!", Toast.LENGTH_SHORT).show();
                    loader.hide();
                    return;
                }
            }

            try{
                if(jsonResponse == null) {
                    throw(new NullPointerException());
                }
                jsonObject = new JSONObject(jsonResponse);

                success = jsonObject.getString("success");
                if(success.equals("true")) {
                    if(s.equals("Waypoint execute") || s.equals("Waypoint pause")) {
                        if (executeWaypointButton.getTag().toString().equals("start")) {
                            executeWaypointButton.setTag("stop");
                            executeWaypointButton.setImageResource(R.mipmap.pause);
                        } else if (executeWaypointButton.getTag().toString().equals("stop")) {
                            executeWaypointButton.setTag("start");
                            executeWaypointButton.setImageResource(R.mipmap.execute_waypoint);
                        }
                    }

                    if(s.equals("Waypoint clear")) {
                        isWayPointSet = false;
                        total_distance_current_waypoint = 0;
                        totalDistance.setText("Dis: 0 m");
                        mMap.clear();
                    }

                    Toast.makeText(getApplicationContext(),s+" successfull",Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(),s+ " unsuccessfull",Toast.LENGTH_SHORT).show();
                }
            }
            catch (NullPointerException e){
                Toast.makeText(getApplicationContext(),"Null pointer",Toast.LENGTH_SHORT).show();
            } catch (JSONException e) {
                Toast.makeText(getApplicationContext(),"Command Rejected.",Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
            loader.hide();
        }
    }

    class getGlobalPosition extends AsyncTask<String,String,String> {
        HttpResponse response;
        BufferedReader reader;
        String jsonResponse;
        JSONObject jsonObject;
        String status, service;

        @Override
        protected String doInBackground(String... strings) {
            try {
                Log.e("IP",url + global_pos);
                response = httpClient.execute(new HttpGet(url + global_pos));

                reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(),"UTF-8"));
                jsonResponse = reader.readLine();

            } catch (IllegalArgumentException e) {
                return "wrongip";
            } catch (NullPointerException e) {
                return "error";
            } catch (MalformedURLException e) {
                return "error";
            } catch (HttpHostConnectException e) {
                e.printStackTrace();
                return "failed";
            } catch (IOException e) {
                e.printStackTrace();
            }
            return "success";
        }

        @Override
        protected void onPostExecute(String s) {
            if(s != null) {
                if(s.equals("failed")) {
                    gpsStatus.setText("No GPS");
                    return;
                }
            }

            try{
                if(jsonResponse == null) {
                    throw(new NullPointerException());
                }
                jsonObject = new JSONObject(jsonResponse);
                droneLat = jsonObject.getDouble("latitude");
                droneLng = jsonObject.getDouble("longitude");

                JSONObject statusObject = new JSONObject(jsonObject.getString("status"));
                status = statusObject.getString("status");
                service = statusObject.getString("service");
                satellite.setText("Satellites: "+status);

                if(droneLat == null || droneLng == null) {
                    gpsStatus.setText("No Position.");
                    satellite.setText("Satellites: 0");
                    return;
                }
                if(service.equals("-1")) {
                    gpsStatus.setText("No FIX");
                    return;
                } else if(service.equals("0")) {
                    gpsStatus.setText("FIX");
                } else if(service.equals("1")) {
                    gpsStatus.setText("GPS Lock");
                } else if(service.equals("2")) {
                    gpsStatus.setText("GBAS FIX");
                } else {
                    gpsStatus.setText("No GPS");
                }
            }
            catch (NullPointerException e){
                gpsStatus.setText("No GPS");
                satellite.setText("Satellites: 0");
            } catch (JSONException e) {
                try {
                    if(reader != null) {
                        reader.close();
                    }
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
                e.printStackTrace();
            }
            setDronePositionOnMap();
        }
    }

    protected void askToStoreRecordedWaypoint() {
        // get prompts.xml view
        LayoutInflater layoutInflater = LayoutInflater.from(PathFlow.this);
        View promptView = layoutInflater.inflate(R.layout.store_waypoint,null,true);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(PathFlow.this);
        alertDialogBuilder.setView(promptView);

        // setup a dialog window
        alertDialogBuilder.setTitle(dialogMessage);
        alertDialogBuilder.setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        createWayPointJSONObject(finalArray);
                    }
                })
                .setNegativeButton("No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                if(mMap != null) {
                                    mMap.clear();
                                }
                            }
                        });

        // create an alert dialog
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

    public static void setWindowFlag(Activity activity, final int bits, boolean on) {
        Window win = activity.getWindow();
        WindowManager.LayoutParams winParams = win.getAttributes();
        if (on) {
            winParams.flags |= bits;
        } else {
            winParams.flags &= ~bits;
        }
        win.setAttributes(winParams);
    }


    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        if (hasFocus)
            hideSystemUI();
    }

    // This snippet hides the system bars.
    private void hideSystemUI() {
        // Set the IMMERSIVE flag.
        // Set the content to appear under the system bars so that the content
        // doesn't resize when the system bars hide and show.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            mDecorView.setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                            | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                            | View.SYSTEM_UI_FLAG_IMMERSIVE
            );
        } /*Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB*/
    }
//
//    // This snippet shows the system bars. It does this by removing all the flags
//    // except for the ones that make the content appear under the system bars.
    private void showSystemUI() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            mDecorView.setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            );
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        globalPosHandler.removeCallbacks(globalPosRunnable);
        finish();
    }

    @Override
    public void onBackPressed() {

        AlertDialog.Builder builder=new AlertDialog.Builder(this);

        builder.setTitle("Are you sure you want to exit?")
                .setPositiveButton("yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //MainActivity.this.onBackPressed();
                        PathFlow.super.onBackPressed();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        AlertDialog alertDialog=builder.create();
        alertDialog.show();
    }
}
