package com.example.jagdish.remoteview;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

public class ObjectFlow extends AppCompatActivity {

    static boolean follow;
    HttpClient httpClient;
    HttpPost httpPost;
    HttpResponse httpResponse;
    HttpEntity httpEntity;
    InputStream is;
    JSONObject jObj = new JSONObject();
    SharedPreferences remoteSetting;
    private View mDecorView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_object_flow);
        final WebView webView = (WebView) findViewById(R.id.webby);
        webView.getSettings().setJavaScriptEnabled(true);
        remoteSetting = getApplicationContext().getSharedPreferences("RemoteSetting", MODE_PRIVATE);
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                webView.loadUrl("javascript:getImage(\"" + remoteSetting.getString("ip", "").trim() + "\");");
                Log.e("MyApplication", "Js added");
            }

//            public void onConsoleMessage(String message, int lineNumber, String sourceID) {
//                Log.e("MyApplication", message+"\t"+ lineNumber+"\t"+sourceID );
//            }


        });
        webView.loadUrl("file:///android_asset/static/newIndex.html");
        final Animation animationFadeIn = AnimationUtils.loadAnimation(this, R.anim.fade_in);
        final ImageButton followButton = (ImageButton) findViewById(R.id.followButton);
        final RelativeLayout followBack = (RelativeLayout) findViewById(R.id.followBack);
        final TextView textView = (TextView) findViewById(R.id.followText);

        new vison_apps().execute("start");
        new follow_track().execute("0");
        follow = false;
        webView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                followBack.setVisibility(View.VISIBLE);
                followButton.startAnimation(animationFadeIn);
                return true;
            }
        });

        followBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                followBack.setVisibility(View.GONE);
            }
        });
        followButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!follow) {
                    new follow_track().execute("1");
                    followButton.setImageDrawable(getResources().getDrawable(R.drawable.stop));
                    followBack.setVisibility(View.GONE);
                    textView.setText("Stop Follow");
                    follow = true;
                } else {
                    follow = false;
                    new follow_track().execute("0");
                    followButton.setImageDrawable(getResources().getDrawable(R.drawable.start));
                    textView.setText("Start Follow");
                    followBack.setVisibility(View.GONE);
                }
            }
        });
        mDecorView = getWindow().getDecorView();
        mDecorView.setOnSystemUiVisibilityChangeListener
                ( new View.OnSystemUiVisibilityChangeListener() {
                    @Override
                    public void onSystemUiVisibilityChange(int visibility) {
                        // Note that system bars will only be "visible" if none of the
                        // LOW_PROFILE, HIDE_NAVIGATION, or FULLSCREEN flags are set.
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                hideSystemUI();
                            }
                        }, 1500);
                    }
                });
        mDecorView.setKeepScreenOn(true);
        hideSystemUI();
    }

    class vison_apps extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... params) {
            httpClient = new DefaultHttpClient();
            httpPost = new HttpPost("http://192.168.137.198/ros/flytpod/navigation/exec_script");
            String check = "{\"app_name\" : \"" + params[0] + "_vision.py\",\"arguments\" : \"\" }";
            Log.e("Params", check);
            httpPost.setEntity(new ByteArrayEntity(check.getBytes()));

            try {
                httpResponse = httpClient.execute(httpPost);
                httpEntity = httpResponse.getEntity();
                is = httpEntity.getContent();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);

                StringBuilder sb = new StringBuilder();
                String line = new String();
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                is.close();

                Log.e("Reply", sb.toString());
                jObj = new JSONObject(sb.toString());
                if (jObj.getBoolean("success"))
                    Toast.makeText(getApplicationContext(), "Vision App started", Toast.LENGTH_LONG).show();

                else
                    Toast.makeText(getApplicationContext(), "Vision Already running", Toast.LENGTH_LONG).show();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    class follow_track extends AsyncTask<String, String, String> {


        @Override
        protected String doInBackground(String... params) {
            httpClient = new DefaultHttpClient();
            httpPost = new HttpPost("http://192.168.137.198/ros/flytpod/param/param_set");
            String check = "{\"param_info\": {\"param_value\": \"" + params[0] + "\", \"param_id\": \"ob_track_follow\"}}";
            Log.e("Params", check);
            httpPost.setEntity(new ByteArrayEntity(check.getBytes()));

            try {
                httpResponse = httpClient.execute(httpPost);
                httpEntity = httpResponse.getEntity();
                is = httpEntity.getContent();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return params[0];
        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);

                StringBuilder sb = new StringBuilder();
                String line = new String();
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                is.close();


                jObj = new JSONObject(sb.toString());

                // return JSON String

                if (jObj.getBoolean("success")) {
                    if (s == "1") {
                        Toast.makeText(getApplicationContext(), "Object Follow Start", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getApplicationContext(), "Object Follow Stop", Toast.LENGTH_LONG).show();
                    }

                } else {
                    Toast.makeText(getApplicationContext(), "Something went wrong", Toast.LENGTH_LONG).show();
                }
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


    }



    @Override
    public void onBackPressed() {
        super.onBackPressed();
        new vison_apps().execute("stop");
        Intent setting = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(setting);
        finish();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        if (hasFocus)
            hideSystemUI();
    }

    // This snippet hides the system bars.
    private void hideSystemUI() {
        // Set the IMMERSIVE flag.
        // Set the content to appear under the system bars so that the content
        // doesn't resize when the system bars hide and show.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            mDecorView.setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                            | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                            | View.SYSTEM_UI_FLAG_IMMERSIVE
            );
        } /*Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB*/
    }
    //
//    // This snippet shows the system bars. It does this by removing all the flags
//    // except for the ones that make the content appear under the system bars.
    private void showSystemUI() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            mDecorView.setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            );
        }
    }

}