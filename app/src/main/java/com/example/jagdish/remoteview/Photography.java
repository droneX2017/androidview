package com.example.jagdish.remoteview;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Photography extends AppCompatActivity {

    private View mDecorView;
    private ImageButton back;
    private ImageButton photo, videostart;
    private ImageView captureImage;
    /*AVLoadingIndicatorView loader;*/

    SharedPreferences remoteSetting;
    String userEnterUrl;
    String url;
    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");

    FrameLayout.LayoutParams ll;
    WebView web;

    HttpClient httpClient;
    HttpResponse response;
    String urlsnapshot;
    String urlvideo;
    String return_message;
    boolean storevideo = false;

    String root = Environment.getExternalStorageDirectory().toString();
    File myDirphoto = new File(root + "/DroneX/photo");
    File myDirvideo = new File(root + "/DroneX/video");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photography);

        //------------------------------------------------------------------Screen Rendering section
        /*loader = (AVLoadingIndicatorView) findViewById(R.id.laoder);
        loader.smoothToShow();*/
        //------------------------------------------------------------Screen Rendering section ended

        //make full transparent statusBar
        if (Build.VERSION.SDK_INT >= 19 && Build.VERSION.SDK_INT < 21) {
            setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, true);
            setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION,true);
        }
        if (Build.VERSION.SDK_INT >= 21) {
            setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, true);
            setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION,true);
            //getWindow().setStatusBarColor(Color.TRANSPARENT);
            //getWindow().setNavigationBarColor(Color.TRANSPARENT);
        }

        //--------------------------------------------------------------------initialization section
        url = getResources().getString(R.string.ip);
        //back = (ImageButton)findViewById(R.id.back);
        photo = (ImageButton)findViewById(R.id.photo);
        videostart = (ImageButton)findViewById(R.id.videostart);
        captureImage = (ImageView) findViewById(R.id.captureImage);
        captureImage.bringToFront();
        httpClient = new DefaultHttpClient();

        remoteSetting = getApplicationContext().getSharedPreferences("RemoteSetting",MODE_PRIVATE);
        userEnterUrl = "http://"+remoteSetting.getString("ip","").trim();
        if(url.equals(userEnterUrl)) {
            urlsnapshot = ""+url+""+getResources().getString(R.string.port)+""+getResources().getString(R.string.snapshot);
            urlvideo = ""+url+""+getResources().getString(R.string.port)+""+getResources().getString(R.string.videostream);
        } else {
            urlsnapshot = ""+userEnterUrl+""+getResources().getString(R.string.port)+""+getResources().getString(R.string.snapshot);
            urlvideo = ""+userEnterUrl+""+getResources().getString(R.string.port)+""+getResources().getString(R.string.videostream);
        }

        /*videostop = (ImageButton)findViewById(R.id.videostop);
        if(videostop != null) {
            videostop.setVisibility(View.GONE);
        }*/ /*videostop != null*/
        //--------------------------------------------------------------initialization section ended

        //-------------------------------------------------------------------Button Listener section
        /*Back to main activity*/
//        if (back != null) {
//            back.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    storevideo = false;
//                    Intent setting = new Intent(getApplicationContext(), MainActivity.class);
//                    startActivity(setting);
//                }
//            });
//        } /*back != null*/

        /*To capture photo
        * Called when photo button is clicked*/
        if(photo != null) {
            photo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new takeSnapshot().execute();
                }
            });
        } /*photo != null*/

        /*To start capture of video
        * Called when videostart button is clicked
        * and to stop capture a video*/
        if(videostart != null) {
            videostart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                if(videostart.getTag().toString().equals("start")) {
                    Toast.makeText(getApplicationContext(),"Start Video",Toast.LENGTH_SHORT).show();
                    /*if(videostop != null) {
                        videostart.setVisibility(View.GONE);
                        videostop.setVisibility(View.VISIBLE);
                    } *//*videostop != null*/

                    videostart.setImageResource(R.mipmap.videostop);
                    videostart.setTag(new String("stop"));
                    if(photo != null) {
                        photo.setVisibility(View.GONE);
                    } /*photo != null*/

                    storevideo = true;
                    new recordVideo().execute();
                } else if(videostart.getTag().toString().equals("stop")) {
                    /*if(videostart != null) {
                        videostop.setVisibility(View.GONE);
                        videostart.setVisibility(View.VISIBLE);
                    } *//**//*videostop != null*//**//**/
                    storevideo = false;
                    videostart.setImageResource(R.mipmap.video);
                    videostart.setTag(new String("start"));
                    if(photo != null) {
                        photo.setVisibility(View.VISIBLE);
                    } /**///*photo != null*//**//**/
                }
                }
            });
        }/*videostart != null*/

        /*To stop capturing of video
        * Called when videostop buttton is clicked*/
        /*if(videostop != null) {
            videostop.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(getApplicationContext(),"Stop Video",Toast.LENGTH_SHORT).show();
                    if(videostart != null) {
                        videostop.setVisibility(View.GONE);
                        videostart.setVisibility(View.VISIBLE);
                    } *//*videostop != null*//*

                    if(photo != null) {
                        photo.setVisibility(View.VISIBLE);
                    } *//*photo != null*//*
                }
            });
        } *//*videostop != null*/
        //-------------------------------------------------------------Button Listener section ended

        //===========================// Video Streaming area //============================================//
        String video = "<body style=\"margin:0px; padding:0px; width:100%; height:100%;\">" +
                "<div  style=\" width:inherit; height:inherit;\"> " +
                "<img  style=\"-webkit-user-select: none; width:inherit; height:inherit;\"  src=\""+urlvideo+"\"/>"+
                "</div>"+
                "</body>";

        final FrameLayout frame = (FrameLayout)findViewById(R.id.frame1);
        ll = new FrameLayout
                .LayoutParams(
                FrameLayout.LayoutParams.FILL_PARENT,
                FrameLayout.LayoutParams.FILL_PARENT);
        web = new WebView(getApplicationContext());
        web.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        web.setBackgroundColor(Color.BLACK);
        web.setLayoutParams(ll);
        //web.getSettings().setLoadWithOverviewMode(true);
        //web.getSettings().setUseWideViewPort(true);
        // web.getSettings().setJavaScriptEnabled(true);
        //web.getSettings().setBuiltInZoomControls(true);
        //web.getSettings().setSupportZoom(true);
        web.loadData(video,"text/html","UTF-8");
        frame.addView(web);// <--- Key line
        //===========================// Video Streaming area ended //============================================//

        //handling auto hide of status bar and navigation bar
        mDecorView = getWindow().getDecorView();
        mDecorView.setOnSystemUiVisibilityChangeListener
                (new View.OnSystemUiVisibilityChangeListener() {
                    @Override
                    public void onSystemUiVisibilityChange(int visibility) {
                        // Note that system bars will only be "visible" if none of the
                        // LOW_PROFILE, HIDE_NAVIGATION, or FULLSCREEN flags are set.
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                hideSystemUI();
                            }
                        }, 1500);
                    }
                });
       mDecorView.setKeepScreenOn(true);

        //hideSystemUI();
        /*loader.smoothToHide();*/
    }

    public static void setWindowFlag(Activity activity, final int bits, boolean on) {
        Window win = activity.getWindow();
        WindowManager.LayoutParams winParams = win.getAttributes();
        if (on) {
            winParams.flags |= bits;
        } else {
            winParams.flags &= ~bits;
        }
        win.setAttributes(winParams);
    }
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        if (hasFocus)
            hideSystemUI();
    }

    private void hideSystemUI() {
        // Set the IMMERSIVE flag.
        // Set the content to appear under the system bars so that the content
        // doesn't resize when the system bars hide and show.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            mDecorView.setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                            | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                            | View.SYSTEM_UI_FLAG_IMMERSIVE
            );
        }
    }

    // This snippet shows the system bars. It does this by removing all the flags
    // except for the ones that make the content appear under the system bars.
    private void showSystemUI() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            mDecorView.setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            );
        }
    }

    class takeSnapshot extends AsyncTask<String,String,String> {
        Bitmap bmp;

        @Override
        protected String doInBackground(String... strings) {
            // response = httpClient.execute(new HttpGet(strings[0]));
            try {
                URL url = new URL(urlsnapshot);
                bmp =  BitmapFactory.decodeStream(url.openConnection().getInputStream());

            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            myDirphoto.mkdirs();
            /*String currentDateandTime = sdf.format(new Date());*/
            String fname = "Image-"+ sdf.format(new Date()) +".jpg";
            File file = new File (myDirphoto, fname);
            try {
                FileOutputStream out = new FileOutputStream(file);
                bmp.compress(Bitmap.CompressFormat.JPEG, 100, out);
                out.flush();
                out.close();
                Toast.makeText(getApplicationContext(), "Photo store successfully", Toast.LENGTH_SHORT).show();
            } catch (Exception e) {
                e.printStackTrace();
            }

            captureImage.setImageBitmap(bmp);
            Log.e("Image","Imageget");
        }
    }

    class recordVideo extends AsyncTask<String,String,String> {

        @Override
        protected String doInBackground(String... strings) {
            myDirvideo.mkdirs();
            String fname = "Video-"+ sdf.format(new Date()) +".mjpeg";
            File file = new File (myDirvideo, fname);
            try {
                URL url = new URL(urlvideo);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.setDoOutput(true);

                urlConnection.connect();

                FileOutputStream out = new FileOutputStream(file);
                InputStream urlInputStream = urlConnection.getInputStream();

                byte[] buffer = new byte[1024];
                int len1 = 0;
                while ((len1 = urlInputStream.read(buffer)) > 0 && storevideo) {
                    out.write(buffer, 0, len1);
                }

                out.close();
                urlConnection.disconnect();
            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }catch (IOException e){
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            Toast.makeText(getApplicationContext(), "Video store successfully", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        storevideo = false;
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        storevideo = false;
        Intent setting = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(setting);
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        storevideo = false;
        finish();
    }
}
