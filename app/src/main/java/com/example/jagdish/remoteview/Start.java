package com.example.jagdish.remoteview;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.wang.avi.AVLoadingIndicatorView;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.HttpHostConnectException;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;

public class Start extends AppCompatActivity {

    private View mDecorView;
    SharedPreferences remoteSetting;
    SharedPreferences.Editor editor;
    private EditText ip;
    private String final_ip;
    private ImageButton connect;
    private TextView requestStatus;
    HttpClient httpClient;
    private String ipUrl, userEnterIpUrl;
    private String nameSpaceUrl, vehicleStateUrl;
    private String nameSpace;
    //TextInputLayout ip;
    AVLoadingIndicatorView startLoader;
    Boolean check = true;

    String TAG = "Start";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //super.recreate();

        Log.w(TAG, "Create New");
        setContentView(R.layout.activity_start);

        //make full transparent statusBar
        if (Build.VERSION.SDK_INT >= 19 && Build.VERSION.SDK_INT < 21) {
            setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, true);
            setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION, true);
        }
        if (Build.VERSION.SDK_INT >= 21) {
            setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, true);
            setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION, true);
        }

        //--------------------------------------------------------------------initialization section
        startLoader = (AVLoadingIndicatorView) findViewById(R.id.startlaoder);
        //startLoader.hide();
        ip = (EditText) findViewById(R.id.ip);
        connect = (ImageButton) findViewById(R.id.connect);
        requestStatus = (TextView) findViewById(R.id.requestStatus);

        ipUrl = getResources().getString(R.string.ip);
        nameSpaceUrl = getResources().getString(R.string.nameSpace);
        vehicleStateUrl = getResources().getString(R.string.vehicleState);

        remoteSetting = getApplicationContext().getSharedPreferences("RemoteSetting", MODE_PRIVATE);
        editor = remoteSetting.edit();
        //--------------------------------------------------------------initialization section ended

        //-------------------------------------------------------------------Button listener section
        //ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        //NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        WifiManager mWifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        int apState = 0;
        try {
            apState = (Integer) mWifiManager.getClass().getMethod("getWifiApState").invoke(mWifiManager);
            //Log.e(TAG,"ApState :"+apState);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }

        // Log.e(TAG,"Wifi Status"+mWifiManager.getWifiState());
        int status = mWifiManager.getWifiState();
        //Log.e(TAG,"Status"+status);
        if (apState == 13 || status == 3) {
            //Log.e(TAG,"Wifi enable2");
            final_ip = remoteSetting.getString("ip", null);
            // Log.e(TAG,"IP"+final_ip);
            if (final_ip == null) {
                //  Log.e(TAG,"Call ip is Null"+final_ip);
                ip_not_found();
            } else {
                //Log.e(TAG,"Call else"+final_ip);
                view_Gone();
                startLoader.show();
                String ip_get = final_ip;
                if (ip_get.isEmpty()) {
                    //Toast.makeText(getApplicationContext(),"Please enter IP",Toast.LENGTH_SHORT).show();
                    requestStatus.setText("Please enter IP");
                    requestStatusDisappear();
                    return;
                }
                userEnterIpUrl = "http://" + ip_get;
                if (!userEnterIpUrl.equals(ipUrl)) {
                    ipUrl = userEnterIpUrl;
                }
                editor.putString("ip", ip_get);
                editor.commit();
                new getNameSpace().execute();
            }
        } else {
            startLoader.hide();
            requestStatus.setText("Please make sure,your DroneX connected to Network");
        }
        //-------------------------------------------------------------Button listener section ended
        mDecorView = getWindow().getDecorView();
        mDecorView.setOnSystemUiVisibilityChangeListener
                (new View.OnSystemUiVisibilityChangeListener() {
                    @Override
                    public void onSystemUiVisibilityChange(int visibility) {
                        // Note that system bars will only be "visible" if none of the
                        // LOW_PROFILE, HIDE_NAVIGATION, or FULLSCREEN flags are set.
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                hideSystemUI();
                            }
                        }, 1500);
                    }
                });
        mDecorView.setKeepScreenOn(true);
        hideSystemUI();
    }

    public void view_Gone() {

        ip.setVisibility(View.GONE);
        connect.setVisibility(View.GONE);
        requestStatus.setVisibility(View.GONE);
    }

    public void view_Visible() {
        ip.setVisibility(View.VISIBLE);
        connect.setVisibility(View.VISIBLE);
        requestStatus.setVisibility(View.VISIBLE);
    }

    public void ip_not_found() {
        connect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String ip_get = ip.getText().toString().trim();
                //Log.e(TAG,"Ip get"+ip_get);
                if (ip_get.isEmpty()) {
                    //Toast.makeText(getApplicationContext(),"Please enter IP",Toast.LENGTH_SHORT).show();
                    requestStatus.setText("Please enter IP");
                    requestStatusDisappear();
                    return;
                }
                userEnterIpUrl = "http://" + ip_get;
                if (!userEnterIpUrl.equals(ipUrl)) {
                    ipUrl = userEnterIpUrl;
                }
                editor.putString("ip", ip_get);
                editor.commit();
                new getNameSpace().execute();
                view_Gone();
                startLoader.show();
            }

        });
    }

    class getNameSpace extends AsyncTask<String, String, String> {

        HttpResponse response;
        BufferedReader reader;
        String jsonResponse;
        JSONObject jsonObject;
        String success;

        @Override
        protected void onPreExecute() {
            //Toast.makeText(getApplicationContext(),"Connecting to Vehicle...",Toast.LENGTH_SHORT).show();
            requestStatus.setText("Connecting to Vehicle...");
            requestStatusDisappear();
            //Log.e(TAG,"call onPreExecute");
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                //Log.e("IP",ipUrl + nameSpaceUrl);
                HttpParams httpParams = new BasicHttpParams();
                HttpConnectionParams.setConnectionTimeout(httpParams, 5000);
                HttpConnectionParams.setSoTimeout(httpParams, 5000);
                httpClient = new DefaultHttpClient(httpParams);
                response = httpClient.execute(new HttpGet(ipUrl + nameSpaceUrl));
                reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
                jsonResponse = reader.readLine();
                //Log.e(TAG,"call DoInBackground");
            } catch (IllegalArgumentException e) {
                return "wrongip";
            } catch (NullPointerException e) {
                return "error";
            } catch (MalformedURLException e) {
                return "error";
            } catch (HttpHostConnectException e) {
                e.printStackTrace();
                return "failed";
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            //Log.e(TAG,"call onPostExecute");
            if (s != null) {
                if (s.equals("error")) {
                    //Toast.makeText(getApplicationContext(), "Sorry something went wrong!\nPlease check IP", Toast.LENGTH_SHORT).show();
                    view_Visible();
                    requestStatus.setText("Sorry something went wrong!\n" + "Please check IP");
                    requestStatusDisappear();
                    startLoader.hide();
                    ip_not_found();
                    return;
                }
                if (s.equals("wrongip")) {
                    //Toast.makeText(getApplicationContext(), "Please enter a valid IP", Toast.LENGTH_SHORT).show();
                    view_Visible();
                    requestStatus.setText("Please enter a valid IP");
                    requestStatusDisappear();
                    startLoader.hide();
                    ip_not_found();
                    return;
                }
                if (s.equals("failed")) {
                    //Toast.makeText(getApplicationContext(), "Connection refused", Toast.LENGTH_SHORT).show();
                    view_Visible();
                    //Log.e(TAG,"call set Visible");
                    requestStatus.setText("Connection refused!!!");
                    requestStatusDisappear();
                    startLoader.hide();
                    ip_not_found();
                    return;
                }
            }

            try {
                //Log.e(TAG,"call Json Response "+jsonResponse);
                if (jsonResponse == null) {
                    throw (new NullPointerException());
                }
                jsonObject = new JSONObject(jsonResponse);
                success = jsonObject.getString("success");
                //Log.e("Success",success);
                if (success.equals("true")) {
                    String param_info = jsonObject.getString("param_info");
                    jsonObject = new JSONObject(param_info);
                    nameSpace = jsonObject.getString("param_value");

                    if (nameSpace.equals("flytpod")) {
                        editor.putString("nameSpace", nameSpace);
                        editor.commit();

                        openMainActivity();
                    } else {
                        //Toast.makeText(getApplicationContext(),"Connection failed!!!",Toast.LENGTH_SHORT).show();
                        //Log.e(TAG,"call Connection failed!!! 1");
                        view_Visible();
                        ip_not_found();
                        requestStatus.setText("Connection failed!!!");
                        requestStatusDisappear();
                    }
                } else {
                    //Toast.makeText(getApplicationContext(),"Connection failed!!!",Toast.LENGTH_SHORT).show();
                    //Log.e(TAG,"call Connection failed!!! 2");
                    view_Visible();
                    ip_not_found();
                    requestStatus.setText("Connection failed!!!");
                    requestStatusDisappear();
                }
            } catch (NullPointerException e) {
                startLoader.hide();
                //Toast.makeText(getApplicationContext(),"Server Not Found",Toast.LENGTH_LONG).show();
                view_Visible();
                ip_not_found();
                //Log.e(TAG,"call Connection failed!!! 3");
                requestStatus.setText("Server Not Found");
                requestStatusDisappear();
            } catch (JSONException e) {
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
                e.printStackTrace();
            }
            //Log.e(TAG,"call Connection failed!!! 4");
            view_Visible();
            ip_not_found();
            startLoader.hide();
        }
    }

    //Disappearing requestStatus after 2s
    public void requestStatusDisappear() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                requestStatus.setText("");
            }
        }, 2000);
    }

    //Open main Activity
    public void openMainActivity() {
        Intent main = new Intent(getApplicationContext(), MainActivity.class);
        // main.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        //finish();
        startActivity(main);
        finish();
    }

    public static void setWindowFlag(Activity activity, final int bits, boolean on) {
        Window win = activity.getWindow();
        WindowManager.LayoutParams winParams = win.getAttributes();
        if (on) {
            winParams.flags |= bits;
        } else {
            winParams.flags &= ~bits;
        }
        win.setAttributes(winParams);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        if (hasFocus)
            hideSystemUI();
    }

    //This snippet hides the system bars.
    private void hideSystemUI() {
        // Set the IMMERSIVE flag.
        // Set the content to appear under the system bars so that the content
        // doesn't resize when the system bars hide and show.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            mDecorView.setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                            | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                            | View.SYSTEM_UI_FLAG_IMMERSIVE
            );
        }
    }

    // This snippet shows the system bars. It does this by removing all the flags
    // except for the ones that make the content appear under the system bars.
    private void showSystemUI() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            mDecorView.setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            );
        }
    }

//    @Override
//    protected void onStop() {
//        super.onStop();
//        Log.e(TAG, "The Stop() event");
//        finish();
//    }
//
//    @Override
//    public void onBackPressed() {
//        super.onBackPressed();
//        Log.e(TAG, "The Back() event");
//        //finish();
//        Log.d(TAG, "The Stop() event");
//        finish();
//    }

//    @Override
//    public void onBackPressed() {
//        finish();
//
//        //super.onDestroy();
//        //Log.d(TAG, "The Back() event");
//        //Start.super.onDestroy();
//
//    }
//    @Override
//    protected void onPause() {
//        super.onPause();
//        Log.e(TAG, "The onPause() event");
//    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.e(TAG, "The Destroy() event");
        finish();
    }
}
//    @Override
//    protected void onResume() {
//        super.onResume();
//        finish();
//        Log.e(TAG, "The Resume() event"+check);
//    }
//    @Override
//    protected void onRestart() {
//        super.onRestart();
//        Log.e(TAG, "The Restart() event");
//
//    }
//
//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//        Log.d(TAG, "The Destroy() event");
//        finish();
//    }
//    @Override
//    protected void onResume() {
//        super.onResume();
//        Log.e(TAG,"Connecting on resume");
//        Log.d(TAG, "The Resume() event");
//
//        }
//    @Override
//    protected void onRestart() {
//        Log.e(TAG,"Connecting on resume");
//        Log.d(TAG, "The Restart() event");
//        super.onRestart();
//    }


