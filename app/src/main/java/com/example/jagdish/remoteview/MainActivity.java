package com.example.jagdish.remoteview;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.GridLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.jmedeisis.bugstick.Joystick;
import com.jmedeisis.bugstick.JoystickListener;
import com.nightonke.boommenu.Animation.BoomEnum;
import com.nightonke.boommenu.Animation.EaseEnum;
import com.nightonke.boommenu.BoomButtons.BoomButton;
import com.nightonke.boommenu.BoomButtons.ButtonPlaceEnum;
import com.nightonke.boommenu.BoomButtons.TextOutsideCircleButton;
import com.nightonke.boommenu.BoomMenuButton;
import com.nightonke.boommenu.ButtonEnum;
import com.nightonke.boommenu.OnBoomListener;
import com.nightonke.boommenu.Piece.PiecePlaceEnum;
import com.nightonke.boommenu.Util;
import com.wang.avi.AVLoadingIndicatorView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.HttpHostConnectException;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;

import pl.droidsonroids.gif.GifDrawable;
import pl.droidsonroids.gif.GifImageButton;

public class MainActivity extends AppCompatActivity {

    SharedPreferences getSharedPreferences;
    SharedPreferences.Editor editor;

    int height;
    private View mDecorView;
    String url, arm, disarm, battery;
    String mode_pos_hold, ros_server_service, mavros_server_service, hud;
    String land, takeoff, vehicleState;
    String vehicleMode, vehicleArm;
    String urlvideo;
    String userEnterUrl;
    String altitude;
    String TAG = "mYDeBUG";
    String string_velocity_set;
    String exec_script;

    GridLayout gridView1,gridView2;

    Boolean is_zone1_right = false, is_zone1_left = false, is_zone1_up = false, is_zone1_down = false, is_zone1_right_up = false, is_zone1_right_down = false, is_zone1_left_up = false, is_zone1_left_down = false;
    Boolean is_zone2_right, is_zone2_left, is_zone2_up, is_zone2_down, is_zone2_right_up, is_zone2_right_down, is_zone2_left_up, is_zone2_left_down;
    Boolean is_altitude, is_powerStatus, is_vehicleState, is_hud, is_head_data,is_head_script,is_distance_data;

    static InputStream is = null;

    Handler vehicleStateHandler;
    Runnable vehicleStateRunnable;

    Joystick zone1, zone2;

    TextView status, armStatus, modeStatus, connectedStatus, powerstatus_textview, text_altitude, textview_display_number_of_head,textview_object_distance;

    ImageButton button_land, button_takeoff;
    AVLoadingIndicatorView loader;

    AsyncTask<String, String, String> server_altitude;

    Boolean is_joy_call1 = true;
    Boolean is_joy_call2 = true;
    Boolean is_arm = false, is_disarm = false;

    ImageView drone;

    GifDrawable gifDrawable;
    String mydebug = "MainActivity";
    private ProgressBar pb;
    private String meunItem[] = {"Path Flow", "Object Flow", "Photography", "Settings"};
    BoomMenuButton bmb, bmb_mode;
    //String mode_list_name[] = {"Arm", "Disarm", "RTL", "Pos Hold", "Loiter", "Alt Hold"};

    String mode_list_name[] = {"Arm", "Disarm","Pos Hold"};
    int mode_list_color[] = {Color.RED, Color.BLUE, Color.parseColor("#673AB7"), Color.parseColor("#795548"), Color.GRAY, Color.parseColor("#FF5722")};
    int mode_list_image[] = {R.mipmap.arm, R.mipmap.disarm,R.mipmap.hold};
    int menuItem_image[] = {R.mipmap.path_follow, R.drawable.start, R.mipmap.photography, R.mipmap.settings};

    Velocity velocity;
    Vibrator vibrator;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (Build.VERSION.SDK_INT >= 19 && Build.VERSION.SDK_INT < 21) {
            setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, true);
            setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION, true);
        }
        if (Build.VERSION.SDK_INT >= 21) {
            setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, true);
            setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION, true);
            getWindow().setNavigationBarColor(Color.GRAY);
        }
        //--------------------------------------------------------------------initialization section


        loader = (AVLoadingIndicatorView) findViewById(R.id.laoder);
        loader.hide();


        httpClient = new DefaultHttpClient();
        armStatus = (TextView) findViewById(R.id.armStatus);
        modeStatus = (TextView) findViewById(R.id.modeStatus);
        connectedStatus = (TextView) findViewById(R.id.connectedStatus);
        text_altitude = (TextView) findViewById(R.id.height);
        textview_display_number_of_head = (TextView) findViewById(R.id.textview_display_number_of_head);
        textview_object_distance = (TextView) findViewById(R.id.textview_object_distance);

        button_land = (ImageButton) findViewById(R.id.button_land);
        button_takeoff = (ImageButton) findViewById(R.id.button_takeoff);
        gridView1=(GridLayout)findViewById(R.id.GridLayout1);
        gridView2=(GridLayout)findViewById(R.id.GridLayout2);

        vibrator = (Vibrator) this.getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
        getSharedPreferences = getApplicationContext().getSharedPreferences("RemoteSetting", MODE_PRIVATE);
        editor = getSharedPreferences.edit();
        server_altitude = new server_altitude();


        //editor.putBoolean("is_altitude",false);
        //editor.commit();


        //====================//Import Url From String //=========================//

        userEnterUrl = "http://" + getSharedPreferences.getString("ip", "").trim();
        ros_server_service = getResources().getString(R.string.ros_server_service);
        arm = getResources().getString(R.string.arm);
        disarm = getResources().getString(R.string.disarm);
        url = getResources().getString(R.string.ip);
        if (!url.equals(userEnterUrl)) {
            url = userEnterUrl;
        }
        urlvideo = "" + url + "" + getResources().getString(R.string.port) + "" + getResources().getString(R.string.videostream);
        //Log.e(mydebug, "UrlVideo" + urlvideo);
        land = getResources().getString(R.string.land);
        takeoff = getResources().getString(R.string.takeoff);
        vehicleState = getResources().getString(R.string.vehicleState);
        hud = getResources().getString(R.string.hud);
        exec_script = getResources().getString(R.string.exec_script);


        //=====================//Call AsynTask //================================//


        zone1 = (Joystick) findViewById(R.id.zone1);
        zone2 = (Joystick) findViewById(R.id.zone2);
        //server_velocity =new server_velocity();
        velocity = new Velocity();
        //networkReceiver=new NetworkReceiver();
        string_velocity_set = getResources().getString(R.string.velocity_set);
        //new server_velocity().execute()
        //status=(TextView)findViewById(R.id.flighttime);
        //networkReceiver.abortBroadcast();
        //----------------------------------------------------------------initialization section end

        //------------------------------------------------------------------------------Menu Control


        bmb = (BoomMenuButton) findViewById(R.id.bmb);
        bmb.setBackground(getResources().getDrawable(R.mipmap.apps_list));
        bmb.setButtonEnum(ButtonEnum.TextOutsideCircle);
        //bmb.setBackgroundColor(Color.TRANSPARENT);
        bmb.setNormalColor(Color.TRANSPARENT);
        bmb.setPiecePlaceEnum(PiecePlaceEnum.DOT_4_2);
        bmb.setButtonPlaceEnum(ButtonPlaceEnum.SC_4_2);
        bmb.setDotRadius(Util.dp2px(0));
        bmb.setBoomEnum(BoomEnum.HORIZONTAL_THROW_2);
        bmb.setShowRotateEaseEnum(EaseEnum.EaseOutBack);
        bmb.setShadowColor(Color.TRANSPARENT);
        bmb.setBackPressListened(false);
        bmb.setAlpha((float) 0.7);
        bmb.setHighlightedColor(Color.TRANSPARENT);

        for (int i = 0; i < bmb.getPiecePlaceEnum().pieceNumber(); i++) {
            TextOutsideCircleButton.Builder builder = new TextOutsideCircleButton.Builder()
                    .normalImageRes(menuItem_image[i])
                    .normalText(meunItem[i])
                    .rotateImage(true)
                    .rippleEffect(true)
                    .normalColor(mode_list_color[i]);
            bmb.addBuilder(builder);
        }

        bmb.setOnBoomListener(new OnBoomListener() {
            @Override
            public void onClicked(int index, BoomButton boomButton) {
                // If you have implement listeners for boom-buttons in builders,
                // then you shouldn't add any listener here for duplicate callbacks.
                if (index == 0) {
                    Intent act = new Intent(getApplicationContext(), PathFlow.class);
                    startActivity(act);
                }

                if (index == 1) {
                    Intent act = new Intent(MainActivity.this, ObjectFlow.class);
                    startActivity(act);
                }

                if (index == 2) {
                    Intent act = new Intent(getApplicationContext(), Photography.class);
                    startActivity(act);
                }

                if (index == 3) {
                    /*Intent act = new Intent(getApplicationContext(), PathFlow.class);
                    startActivity(act);*/
                    //  Toast.makeText(getApplication(),"Activity is not set yet",Toast.LENGTH_SHORT).show();
                    Toast.makeText(MainActivity.this, "Settings", Toast.LENGTH_LONG).show();
                    startActivity(new Intent(MainActivity.this, Setting.class));
                    //finish();
                }
            }

            @Override
            public void onBackgroundClick() {
                //Toast.makeText(getApplicationContext(),"Click background!!!",Toast.LENGTH_SHORT).show();
                //bmb.setBackground(getResources().getDrawable(R.mipmap.apps_list));

            }

            @Override
            public void onBoomWillHide() {
                ////Toast.makeText(getApplicationContext(),"Will RE-BOOM!!!",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onBoomDidHide() {
                //Toast.makeText(getApplicationContext(),"Did RE-BOOM!!!",Toast.LENGTH_SHORT).show();
                bmb.setBackground(getResources().getDrawable(R.mipmap.apps_list));

            }

            @Override
            public void onBoomWillShow() {
                //Toast.makeText(getApplicationContext(),"Will BOOM!!!",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onBoomDidShow() {
                //Toast.makeText(getApplicationContext(),"Did BOOM!!!",Toast.LENGTH_SHORT).show();
                bmb.setBackground(null);

            }
        });
        //-----------------------------------------------------------------------end of menu control
        //==========================//powerStatus//=================================//

        mavros_server_service = getResources().getString(R.string.mavros_server_service);
        battery = getResources().getString(R.string.battery);
        pb = (ProgressBar) findViewById(R.id.powerstatus);
        powerstatus_textview = (TextView) findViewById(R.id.powerstatus_textview);

        powerstatus_textview.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        //=============================// Altitude //==============================//

        altitude = getResources().getString(R.string.altitude);

        //---------------------------------------------------------------------------Mode Controller
        bmb_mode = (BoomMenuButton) findViewById(R.id.modebmb);
        bmb_mode.setButtonEnum(ButtonEnum.TextOutsideCircle);
        bmb_mode.setPiecePlaceEnum(PiecePlaceEnum.DOT_3_3);
        bmb_mode.setButtonPlaceEnum(ButtonPlaceEnum.SC_3_3);
        bmb_mode.setDotRadius(Util.dp2px(0));
        bmb_mode.setBackground(getResources().getDrawable(R.mipmap.arm_mode_list));
        bmb_mode.setBoomEnum(BoomEnum.HORIZONTAL_THROW_2);
        bmb_mode.setShowRotateEaseEnum(EaseEnum.EaseOutBack);
        bmb_mode.setShadowColor(Color.TRANSPARENT);
        bmb_mode.setNormalColor(Color.TRANSPARENT);
        bmb_mode.setHighlightedColor(Color.TRANSPARENT);
        bmb_mode.setBackPressListened(false);

        //Toast.makeText(MainActivity.this,"Arm"+url+ros_server_service+arm,Toast.LENGTH_LONG).show();
        for (int i = 0; i < bmb_mode.getPiecePlaceEnum().pieceNumber(); i++) {
            TextOutsideCircleButton.Builder builder = new TextOutsideCircleButton.Builder()
                    .normalText(mode_list_name[i])
                    .normalImageRes(mode_list_image[i])
                    .normalColor(mode_list_color[i]);
            bmb_mode.addBuilder(builder);
        }
        bmb_mode.setOnBoomListener(new OnBoomListener() {
            @Override
            public void onClicked(int index, BoomButton boomButton) {
                if (index == 0) {
                    //Toast.makeText(MainActivity.this,"Arm",Toast.LENGTH_LONG).show();
                    //Log.e(TAG, "url :"+url+ros_server_service+arm);
                    // Log.e(TAG, "Arm :");
//                    if (is_arm) {
//                        Log.e(TAG, "call Arm :");
//                        Log.e(TAG, "url :"+url+ros_server_service+arm);
//                        new server_mode().execute(url + ros_server_service + arm);
//                    }else{
//                        Log.e(TAG, "Call Disrrm :");
//                        Log.e(TAG, "url :"+url+ros_server_service+disarm);
//                        new server_mode().execute(url + ros_server_service + disarm);
                    loader.show();
                    is_arm = true;
                    new server_mode().execute(url + ros_server_service + arm);

                }
                if (index == 1) {
                    //Toast.makeText(MainActivity.this,"Stabilize",Toast.LENGTH_LONG).show();
                    // Log.e(TAG, "Call Disrrm :");
                    // Log.e(TAG, "url :" + url + ros_server_service + disarm);
                    loader.show();
                    is_disarm = true;
                    new server_mode().execute(url + ros_server_service + disarm);
                }
                if (index == 2) {
                    //Toast.makeText(MainActivity.this,"RTL",Toast.LENGTH_LONG).show();
                }
                if (index == 3) {
                    //Toast.makeText(MainActivity.this,"pos_hold",Toast.LENGTH_LONG).show();
                    mode_pos_hold = getResources().getString(R.string.mode_position_hold);
                    Log.e(TAG, "url :" + url + ros_server_service + mode_pos_hold);
                    new server_mode().execute(url + ros_server_service + mode_pos_hold);
                }
                if (index == 4) {
                    //Toast.makeText(MainActivity.this,"Loiter",Toast.LENGTH_LONG).show();
                }
                if (index == 5) {
                    //Toast.makeText(MainActivity.this,"Alt Hold",Toast.LENGTH_LONG).show();
                    startActivity(new Intent(MainActivity.this, Setting.class));
                    finish();
                }
            }

            @Override
            public void onBackgroundClick() {
                //bmb_mode.setBackground(getResources().getDrawable(R.mipmap.arm_mode_list));

            }

            @Override
            public void onBoomWillHide() {

            }

            @Override
            public void onBoomDidHide() {
                bmb_mode.setBackground(getResources().getDrawable(R.mipmap.arm_mode_list));
            }

            @Override
            public void onBoomWillShow() {

            }

            @Override
            public void onBoomDidShow() {
                bmb_mode.setBackground(null);

            }
        });
        //------------------------//Mode controller section end //===============================

        //===========================// Video Streaming area //===================================//

        //new video_status().execute(urlvideo);

        final String video = "<body style=\"margin:0px; padding:0px; width:100%; height:100%;\">" +
                "<div  style=\" width:inherit; height:inherit;\"> " +
                "<img  style=\"-webkit-user-select: none; width:inherit; height:inherit;\"  src=\"" + urlvideo + "\"/>" +
                "</div>" +
                "</body>";

        final FrameLayout frame = (FrameLayout) findViewById(R.id.frame1);
        FrameLayout.LayoutParams ll = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.FILL_PARENT,
                FrameLayout.LayoutParams.FILL_PARENT);

        WebView web = new WebView(getApplicationContext());
        web.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        web.setBackgroundColor(Color.parseColor("#9E9E9E"));
        web.setLayoutParams(ll);
        web.loadData(video, "text/html", "UTF-8");
        frame.addView(web); // <--- Key line
        //===========================// Video Streaming area //===================================//

        drone = (ImageView) findViewById(R.id.orientor);


        final GifImageButton props = (GifImageButton) findViewById(R.id.props);
        gifDrawable = (GifDrawable) props.getDrawable();
        gifDrawable.stop();

//        props.setOnLongClickListener(new View.OnLongClickListener() {
//            @Override
//            public boolean onLongClick(View view) {
//
//                if (gifDrawable.isRunning()){
//                    Log.e("land",url+land);
//                    new land().execute(url+land);
//
//                }else {
//                    Log.e("takeoff",url+takeoff);
//                    showInputDialog();
//                }
//                return true;
//            }
//        });


        button_land.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (gifDrawable.isRunning()) {
                    //Log.e("land",url+land);
                    vibrator.vibrate(500);
                    new land().execute(url + land);

                }
                return true;
            }
        });


        button_takeoff.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Log.e("takeoff", url + takeoff);
                vibrator.vibrate(500);
                showInputDialog();
                return true;
            }
        });

        // ========================== // Joystick Zone1 //=======================================//

        Log.e(mydebug, zone1.getMotionConstraint().toString());

        if (zone1 != null) {
            zone1.setJoystickListener(new JoystickListener() {
                @Override
                public void onDown() {
//                    if (is_zone1_right){
//                        Log.e(mydebug,"Final Right");
//                        velocity.setTwist(0,1,0,0,false);
//                        is_zone1_right=false;
//
//                    }else if (is_zone1_left){
//                        Log.e(mydebug,"Final Left");
//                        velocity.setTwist(0,-1,0,0,false);
//                        is_zone1_left=false;
//                    }else if (is_zone1_up){
//                        Log.e(mydebug,"Final up");
//                        velocity.setTwist(1,0,0,0,false);
//                        is_zone1_up=false;
//
//                    }else if (is_zone1_right_up){
//                        Log.e(mydebug,"Final Right Up");
//                        velocity.setTwist(1,1,0,0,false);
//                        is_zone1_right_up=false;
//
//                    }else if (is_zone1_right_down){
//                        Log.e(mydebug,"Final Right down");
//                        velocity.setTwist(-1,1,0,0,false);
//                        is_zone1_right_down=false;
//
//                    }else if (is_zone1_left_up){
//                        Log.e(mydebug,"Final left up");
//                        velocity.setTwist(1,-1,0,0,false);
//                        is_zone1_left_up=false;
//
//                    }else if (is_zone1_left_down){
//                        Log.e(mydebug,"Final left down");
//                        velocity.setTwist(-1,-1,0,0,false);
//                        is_zone1_left_down=false;
//
//                    }else if (is_zone1_down) {
//                        Log.e(mydebug, "Final down");
//                        velocity.setTwist(-1,0,0,0,false);
//                        is_zone1_down = false;
//                    }
//                    Log.e(mydebug,"Call in On Down");
//                    new server_velocity().execute();
                    is_joy_call1 = true;
                    is_zone1_right = false;
                    is_zone1_left = false;
                    is_zone1_up = false;
                    is_zone1_down = false;
                    is_zone1_right_up = false;
                    is_zone1_right_down = false;
                    is_zone1_left_up = false;
                    is_zone1_left_down = false;
                }

                @Override
                public void onDrag(float degrees, float offset) {

                    if (offset == 1) {

                        if (is_joy_call1) {
                            Log.e(mydebug, "Joy call one time in Ondrag");
                            if (degrees >= 0 && degrees < 22.5) {
                                Log.e(mydebug, "Right");
                                velocity.setTwist(0, 0.5f, 0, 0, false);
                                is_zone1_right = true;

                            } else if (degrees >= 22.5 && degrees < 67.5) {
                                is_zone1_right_up = true;
                                Log.e(mydebug, "Middle of Right + up");
                                velocity.setTwist(0.5f, 0.5f, 0, 0, false);

                            } else if ((degrees >= 67.5 && degrees < 112.5)) {
                                Log.e(mydebug, "Up");
                                velocity.setTwist(0.5f, 0, 0, 0, false);
                                is_zone1_up = true;

                            } else if (degrees >= 112.5 && degrees < 157.5) {
                                Log.e(mydebug, "Middle of Up + left");
                                velocity.setTwist(0.5f, 0.5f, 0, 0, false);
                                is_zone1_left_up = true;

                            } else if (degrees >= 157.5 && degrees < 180) {
                                velocity.setTwist(0, -0.5f, 0, 0, false);
                                Log.e(mydebug, "Left");
                                is_zone1_left = true;

                            } else if (degrees <= -0 && degrees > -22.5) {
                                Log.e(mydebug, "Right");
                                is_zone1_right = true;
                                velocity.setTwist(0, 0.5f, 0, 0, false);

                            } else if (degrees <= -22.5 && degrees > -67.5) {
                                Log.e(mydebug, "Middle of Right + down");
                                is_zone1_right_down = true;
                                velocity.setTwist(-0.5f, 0.5f, 0, 0, false);

                            } else if ((degrees <= -67.5 && degrees > -112.5)) {
                                Log.e(mydebug, "down");
                                velocity.setTwist(-0.5f, 0, 0, 0, false);
                                is_zone1_down = true;

                            } else if (degrees <= -112.5 && degrees > -157.5) {
                                Log.e(mydebug, "Middle of down + left");
                                velocity.setTwist(-0.5f, -0.5f, 0, 0, false);
                                is_zone1_left_down = true;

                            } else if (degrees <= -157.5 && degrees > -180) {
                                Log.e(mydebug, "Left");
                                velocity.setTwist(0, -0.5f, 0, 0, false);
                                is_zone1_left = true;

                            }

                            new server_velocity().execute();
                            is_joy_call1 = false;
                        }
                    }
                }

                @Override
                public void onUp() {
                    //drone.setRotation(0);
//                    if (is_zone1_right){
//                        Log.e(mydebug,"Final Right");
//                        velocity.setTwist(0,1,0,0,false);
//                        is_zone1_right=false;
//
//                    }else if (is_zone1_left){
//                        Log.e(mydebug,"Final Left");
//                        velocity.setTwist(0,-1,0,0,false);
//                        is_zone1_left=false;
//                    }else if (is_zone1_up){
//                        Log.e(mydebug,"Final up");
//                        velocity.setTwist(1,0,0,0,false);
//                        is_zone1_up=false;
//
//                    }else if (is_zone1_right_up){
//                        Log.e(mydebug,"Final Right Up");
//                        velocity.setTwist(1,1,0,0,false);
//                        is_zone1_right_up=false;
//
//                    }else if (is_zone1_right_down){
//                        Log.e(mydebug,"Final Right down");
//                        velocity.setTwist(-1,1,0,0,false);
//                        is_zone1_right_down=false;
//
//                    }else if (is_zone1_left_up){
//                        Log.e(mydebug,"Final left up");
//                        velocity.setTwist(1,-1,0,0,false);
//                        is_zone1_left_up=false;
//
//                    }else if (is_zone1_left_down){
//                        Log.e(mydebug,"Final left down");
//                        velocity.setTwist(-1,-1,0,0,false);
//                        is_zone1_left_down=false;
//
//                    }else if (is_zone1_down) {
//                        Log.e(mydebug, "Final down");
//                        velocity.setTwist(-1,0,0,0,false);
//                        is_zone1_down = false;
//                    }
                    //  Log.e(mydebug,"Call Zone1");
                    //velocity.setTwist(0,0,0,0,false);
                    //new server_velocity().execute();
                }

            });
        }

        // ========================== // Joystick Zone2 //=======================================//

        Log.e(mydebug, zone2.getMotionConstraint().toString());

        if (zone2 != null) {
            zone2.setJoystickListener(new JoystickListener() {
                @Override
                public void onDown() {
                    is_joy_call2 = true;
                    is_zone2_right = false;
                    is_zone2_left = false;
                    is_zone2_up = false;
                    is_zone2_down = false;
                    is_zone2_right_up = false;
                    is_zone2_right_down = false;
                    is_zone2_left_up = false;
                    is_zone2_left_down = false;
                }

                @Override
                public void onDrag(float degrees, float offset) {
                    zone2.setOnTouchListener(new View.OnTouchListener() {
                        @Override
                        public boolean onTouch(View view, MotionEvent me) {
                            return false;
                        }

                    });
                    if (offset == 1) {
                        if (is_joy_call2) {
                            if (degrees >= 0 && degrees < 22.5) {
                                Log.e(mydebug, "Right");
                                velocity.setTwist(0, 0, 0, 0.5, true);
                                is_zone2_right = true;

                            } else if (degrees >= 22.5 && degrees < 67.5) {
                                Log.e(mydebug, "Middle of Right + up");
                                velocity.setTwist(0, 0, -0.5f, 0.5, true);
                                is_zone2_right_up = true;

                            } else if ((degrees >= 67.5 && degrees < 112.5)) {
                                Log.e(mydebug, "Up");
                                velocity.setTwist(0, 0, -0.5f, 0, false);
                                is_zone2_up = true;

                            } else if (degrees >= 112.5 && degrees < 157.5) {
                                Log.e(mydebug, "Middle of Up + left");
                                velocity.setTwist(0, 0, -0.5f, -0.5, true);
                                is_zone2_left_up = true;

                            } else if (degrees >= 157.5 && degrees < 180) {
                                Log.e(mydebug, "Left");
                                velocity.setTwist(0, 0, 0, -0.5, true);
                                is_zone2_left = true;

                            } else if (degrees <= -0 && degrees > -22.5) {
                                Log.e(mydebug, "Right");
                                velocity.setTwist(0, 0, 0, 0.5, true);
                                is_zone2_right = true;

                            } else if (degrees <= -22.5 && degrees > -67.5) {
                                Log.e(mydebug, "Middle of Right + down");
                                velocity.setTwist(0, 0, 0.5f, 0.5, true);
                                is_zone2_right_down = true;

                            } else if ((degrees <= -67.5 && degrees > -112.5)) {
                                Log.e(mydebug, "down");
                                velocity.setTwist(0, 0, 0.5f, 0, false);
                                is_zone2_down = true;

                            } else if (degrees <= -112.5 && degrees > -157.5) {
                                Log.e(mydebug, "Middle of down + left");
                                velocity.setTwist(0, 0, 0.5f, -0.5, true);
                                is_zone2_left_down = true;

                            } else if (degrees <= -157.5 && degrees > -180) {
                                Log.e(mydebug, "Left");
                                velocity.setTwist(0, 0, 0, -0.5, true);
                                is_zone2_left = true;

                            }
                            new server_velocity().execute();
                            is_joy_call2 = false;
                        }
                    }
                }

                @Override
                public void onUp() {
//                    drone.setRotation(0);
//                    if (is_zone2_right){
//                        Log.e(mydebug,"Final Right");
//                        velocity.setTwist(0,0,0,0.5,true);
//                        is_zone2_right=false;
//
//                    }else if (is_zone2_left){
//                        Log.e(mydebug,"Final Left");
//                        velocity.setTwist(0,0,0,-0.5,true);
//                        is_zone2_left=false;
//
//                    }else if (is_zone2_up){
//                        Log.e(mydebug,"Final up");
//                        velocity.setTwist(0,0,-1,0,false);
//                        is_zone2_up=false;
//
//                    }else if (is_zone2_right_up){
//                        Log.e(mydebug,"Final Right Up");
//                        velocity.setTwist(0,0,-1,0.5,true);
//                        is_zone2_right_up=false;
//
//                    }else if (is_zone2_right_down){
//                        Log.e(mydebug,"Final Right down");
//                        velocity.setTwist(0,0,1,0.5,true);
//                        is_zone2_right_down=false;
//
//                    }else if (is_zone2_left_up){
//                        Log.e(mydebug,"Final left up");
//                        velocity.setTwist(0,0,-1,-0.5,true);
//                        is_zone2_left_up=false;
//
//                    }else if (is_zone2_left_down){
//                        Log.e(mydebug,"Final left down");
//                        velocity.setTwist(0,0,1,-0.5,true);
//                        is_zone2_left_down=false;
//
//                    }else if (is_zone2_down) {
//                        Log.e(mydebug, "Final down");
//                        velocity.setTwist(0,0,1,0,false);
//                        is_zone2_down = false;
//                    }
//                    Log.e(mydebug,"Call Zone2");

                    //velocity.setTwist(0,0,0,0,false);
                    //new server_velocity().execute();

                }
            });
        }

        mDecorView = getWindow().getDecorView();
        mDecorView.setOnSystemUiVisibilityChangeListener
                (new View.OnSystemUiVisibilityChangeListener() {
                    @Override
                    public void onSystemUiVisibilityChange(int visibility) {
                        // Note that system bars will only be "visible" if none of the
                        // LOW_PROFILE, HIDE_NAVIGATION, or FULLSCREEN flags are set.
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                hideSystemUI();
                            }
                        }, 2500);
                    }
                });


        //Log.e(TAG,"Call Share......34..>>!!");

        is_altitude = getSharedPreferences.getBoolean("is_altitude", false);
        is_powerStatus = getSharedPreferences.getBoolean("is_powerStatus", false);
        is_vehicleState = getSharedPreferences.getBoolean("is_vehicleState", false);
        is_hud = getSharedPreferences.getBoolean("is_hud", false);
        is_head_script = getSharedPreferences.getBoolean("is_head_script", false);
        is_head_data = getSharedPreferences.getBoolean("is_head_data", false);
        is_distance_data = getSharedPreferences.getBoolean("is_distance_data", false);



        Log.e(TAG,"is_head_script 1"+is_head_script);

        if (!is_distance_data){
            textview_object_distance.setText("NA");
            gridView2.setBackground(getResources().getDrawable(R.drawable.rectangle_bar_default));

        }
        if (!is_head_data){
            textview_display_number_of_head.setText("NA");
            gridView1.setBackground(getResources().getDrawable(R.drawable.rectangle_bar_default));

        }
        if (is_head_script) {
            new head_apps().execute("start");
        }else{
            new head_apps().execute("stop");
        }

        if (!is_altitude) {
            text_altitude.setText("NA");
            //text_altitude.setBackground(getResources().getDrawable(R.drawable.rectangle_bar_default));
        }
        if (!is_hud) {
            drone.setRotation(0);
        }
        if (!is_powerStatus) {
            powerstatus_textview.setText("No Data");
            powerstatus_textview.setBackground(getResources().getDrawable(R.drawable.rectangle_bar_default));

        }
        if (!is_vehicleState) {
            modeStatus.setText("No Data");
            armStatus.setText("No Data");
            armStatus.setBackground(getResources().getDrawable(R.drawable.rectangle_bar_default));
            modeStatus.setBackground(getResources().getDrawable(R.drawable.rectangle_bar_default));

        }

        Log.e(mydebug, "Shared  Pref Altitude :" + is_altitude);
        Log.e(mydebug, "Shared  Pref powerStatus :" + is_powerStatus);
        Log.e(mydebug, "Shared  Pref Vehicle State :" + is_vehicleState);
        Log.e(mydebug, "Shared  Pref Vehicle State :" + is_hud);

        mDecorView.setKeepScreenOn(true);
        //Get Vehicle status after 1s.
        call_continues_request();
    }

    public void call_continues_request() {
        vehicleStateHandler = new Handler();
        vehicleStateRunnable = new Runnable() {
            @Override
            public void run() {
                is_altitude = getSharedPreferences.getBoolean("is_altitude", false);
                is_powerStatus = getSharedPreferences.getBoolean("is_powerStatus", false);
                is_vehicleState = getSharedPreferences.getBoolean("is_vehicleState", false);
                is_hud = getSharedPreferences.getBoolean("is_hud", false);
                if (is_vehicleState) {
                    new getVehicleState().execute();
                }
                if (is_powerStatus) {
                    new server_powerstatus().execute(userEnterUrl + mavros_server_service + battery);
                }
                if (is_altitude) {
                    new server_altitude().execute(userEnterUrl + mavros_server_service + altitude);
                }
                if (is_hud) {
                    new server_hud().execute(userEnterUrl + mavros_server_service + hud);
                }
                if (is_head_data) {
                    new server_head_data().execute();
                }
//                Log.e(TAG,"Distance"+is_distance_data);
                if (is_distance_data){
                    Log.e(TAG,"Distance :"+is_distance_data);

                    new server_distance_data().execute();
                }
                vehicleStateHandler.postDelayed(this, 2000);
            }
        };
        vehicleStateHandler.postDelayed(vehicleStateRunnable, 2000);
        hideSystemUI();
    }

    public static void setWindowFlag(Activity activity, final int bits, boolean on) {
        Window win = activity.getWindow();
        WindowManager.LayoutParams winParams = win.getAttributes();
        if (on) {
            winParams.flags |= bits;
        } else {
            winParams.flags &= ~bits;
        }
        win.setAttributes(winParams);
    }

 private void hideSystemUI() {
        // Set the IMMERSIVE flag.
        // Set the content to appear under the system bars so that the content
        // doesn't resize when the system bars hide and show.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            mDecorView.setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                            | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                            | View.SYSTEM_UI_FLAG_IMMERSIVE
            );
        }
    }

    // This snippet shows the system bars. It does this by removing all the flags
// except for the ones that make the content appear under the system bars.
    private void showSystemUI() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            mDecorView.setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            );
        }
    }
    @Override
    protected void onRestart() {
        super.onRestart();
        //Log.e(TAG, "The Restart() event");
        //Log.e(TAG,"Call Share........>>!!");
        is_altitude = getSharedPreferences.getBoolean("is_altitude", false);
        is_powerStatus = getSharedPreferences.getBoolean("is_powerStatus", false);
        is_vehicleState = getSharedPreferences.getBoolean("is_vehicleState", false);
        is_hud = getSharedPreferences.getBoolean("is_hud", false);
        is_head_script = getSharedPreferences.getBoolean("is_head_script", false);
        is_head_data = getSharedPreferences.getBoolean("is_head_data", false);
        is_distance_data = getSharedPreferences.getBoolean("is_distance_data", false);

        vehicleStateHandler.removeCallbacks(vehicleStateRunnable);
        Log.e(TAG,"is_head_script 2"+is_head_script);

        if (is_head_script) {
            new head_apps().execute("start");
        }else if (!is_head_script){
            new head_apps().execute("stop");
        }
        //Log.e(TAG,"Altitude "+is_altitude);
        if (!is_altitude) {
            text_altitude.setText("NA");
            //text_altitude.setBackground(getResources().getDrawable(R.drawable.rectangle_bar_default));

        }
        if (!is_hud) {
            drone.setRotation(0);
        }
        if (!is_powerStatus) {
            powerstatus_textview.setText("No Data");
            powerstatus_textview.setBackground(getResources().getDrawable(R.drawable.rectangle_bar_default));

        }
        if (!is_vehicleState) {
            modeStatus.setText("No Data");
            armStatus.setText("No Data");
            modeStatus.setBackground(getResources().getDrawable(R.drawable.rectangle_bar_default));
            armStatus.setBackground(getResources().getDrawable(R.drawable.rectangle_bar_default));

        }
        if (!is_distance_data){
            textview_object_distance.setText("NA");
            gridView2.setBackground(getResources().getDrawable(R.drawable.rectangle_bar_default));

        }
        if (!is_head_data){
            textview_display_number_of_head.setText("NA");
            gridView1.setBackground(getResources().getDrawable(R.drawable.rectangle_bar_default));

        }
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        call_continues_request();
    }

    @Override
    public void onBackPressed() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle("Are you sure you want to exit?")
                .setPositiveButton("yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //MainActivity.this.onBackPressed();
                        MainActivity.super.onBackPressed();
                        System.exit(0);

//                        try{
//                            MainActivity.super.onDestroy();
//                        }catch (IllegalStateException e){
//                            e.printStackTrace();
//                        }
                        //finish();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    class land extends AsyncTask<String, String, String> {

        String return_message;
        HttpResponse response;

        @Override
        protected void onPreExecute() {
            loader.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                response = httpClient.execute(new HttpGet(url + land));

                ResponseHandler<String> handler = new BasicResponseHandler();
                return_message = handler.handleResponse(response).trim();

                Log.e(mydebug, "return_message: Land:" + return_message);
            } catch (NullPointerException e) {
                return "error";
            } catch (MalformedURLException e) {
                return "error";
            } catch (HttpHostConnectException e) {
                e.printStackTrace();
                return "failed";
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {

            if (s != null) {
                if (s.equals("error")) {
                    Toast.makeText(getApplicationContext(), "Sorry something went wrong!\nPlease check IP", Toast.LENGTH_SHORT).show();
                    loader.hide();
                    return;
                }

                if (s.equals("failed")) {
                    Toast.makeText(getApplicationContext(), "Connection refused", Toast.LENGTH_SHORT).show();
                    loader.hide();
                    return;
                }
            }

            try {
                if (return_message.contains("false")) {
                    Toast.makeText(MainActivity.this, "Land unsuccessful", Toast.LENGTH_LONG).show();
                    /*gifDrawable.start();*/
                } else if (return_message.contains("true")) {
                    Toast.makeText(MainActivity.this, "Land successful", Toast.LENGTH_LONG).show();
                    gifDrawable.stop();
                }
            } catch (NullPointerException e) {
                gifDrawable.start();
                Toast.makeText(getApplicationContext(), "Server Not Found", Toast.LENGTH_LONG).show();
            }
            loader.hide();
        }
    }

    class takeoff extends AsyncTask<String, String, String> {

        String return_message;
        HttpResponse response;

        @Override
        protected void onPreExecute() {
            loader.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                String postMessage = "{\"takeoff_alt\": " + height + "}";
                /*HttpClient httpclient = new DefaultHttpClient();*/
                HttpPost httpPost = new HttpPost(url + takeoff);
                httpPost.setHeader("Content-type", "application/json");

                HttpParams httpParams = new BasicHttpParams();
                HttpClient httpclient = new DefaultHttpClient(httpParams);
                httpPost.setEntity(new ByteArrayEntity(
                        postMessage.toString().getBytes("UTF8")));

                /*List<NameValuePair> dataArray = new ArrayList<NameValuePair>(1);
                dataArray.add(new BasicNameValuePair("takeoff_alt", String.valueOf(3)));
                httpPost.setEntity(new UrlEncodedFormEntity(dataArray));*/

                response = httpclient.execute(httpPost);

                ResponseHandler<String> handler = new BasicResponseHandler();
                return_message = handler.handleResponse(response).trim();

                Log.e(mydebug, "return_message: Takeoff:" + return_message);
            } catch (NullPointerException e) {
                return "error";
            } catch (MalformedURLException e) {
                return "error";
            } catch (HttpHostConnectException e) {
                e.printStackTrace();
                return "failed";
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {

            if (s != null) {
                if (s.equals("error")) {
                    Toast.makeText(getApplicationContext(), "Sorry something went wrong!\nPlease check IP", Toast.LENGTH_SHORT).show();
                    loader.hide();
                    return;
                }

                if (s.equals("failed")) {
                    Toast.makeText(getApplicationContext(), "Connection refused", Toast.LENGTH_SHORT).show();
                    loader.hide();
                    return;
                }
            }

            try {
                if (return_message.contains("false")) {
                    Toast.makeText(MainActivity.this, "Takeoff unsuccessful", Toast.LENGTH_LONG).show();
                } else if (return_message.contains("true")) {
                    gifDrawable.start();
                    Toast.makeText(MainActivity.this, "Takeoff successful", Toast.LENGTH_LONG).show();
                }
            } catch (NullPointerException e) {
                Toast.makeText(getApplicationContext(), "Server Not Found", Toast.LENGTH_LONG).show();
                gifDrawable.stop();
            }
            loader.hide();
        }
    }

    class server_mode extends AsyncTask<String, String, String> {
        HttpResponse response;
        BufferedReader reader;
        String jsonResponse;
        JSONObject jsonObject;

        @Override
        protected String doInBackground(String... strings) {
            try {
                response = httpClient.execute(new HttpGet(strings[0]));
                reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
                jsonResponse = reader.readLine();

            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            try {
                jsonObject = new JSONObject(jsonResponse);
                // Log.e(TAG,"Success Result "+jsonObject.getString("success"));

                String myResult = jsonObject.getString("success");
                //Log.e(TAG,"Success "+myResult);

                if ("true" == myResult && is_arm.equals(true)) {
                    //mode_list_name.add(0,"ARM");
                    //bmb_mode.clearBuilders();
//                    Toast.makeText(MainActivity.this,myResult+",Armed",Toast.LENGTH_LONG).show();
//                    bmb_mode.setBuilder(0,new TextOutsideCircleButton.Builder()
//                            .normalText("Disarm")
//                            .normalImageRes(R.drawable.disarm)
//                            .normalColor(Color.parseColor("#8BC34A")));
//                    bmb_mode.isReBoomed();
                    loader.hide();
                    is_arm = false;
                    //Log.e(TAG, " Arm");
                } else if ("true".equals(myResult) && is_disarm.equals(true)) {
//                    Toast.makeText(MainActivity.this,myResult+", Disarm ",Toast.LENGTH_LONG).show();
//                    bmb_mode.setBuilder(0,new TextOutsideCircleButton.Builder()
//                            .normalText("Arm")
//                            .normalImageRes(R.drawable.arm)
//
//              .normalColor(Color.RED));
//                    bmb_mode.isReBoomed();
                    loader.hide();
                    is_disarm = false;
                    // Log.e(TAG, "Disarm");
                } else if ("false" == myResult && is_arm.equals(true)) {
                    loader.hide();
                    is_arm = false;
                    Toast.makeText(MainActivity.this, myResult + "Something Wrong,Arm not SuccessFull ", Toast.LENGTH_LONG).show();
                } else if ("false" == myResult && is_disarm.equals(true)) {
                    Toast.makeText(MainActivity.this, myResult + "Something Wrong,Arm not SuccessFull ", Toast.LENGTH_LONG).show();
                    loader.hide();
                    is_disarm = false;
                }
            } catch (JSONException e) {
                e.printStackTrace();
                Toast.makeText(MainActivity.this, "Something Wrong !! try Again ", Toast.LENGTH_LONG).show();
                loader.hide();
                is_disarm = false;
                is_arm = false;
            }
            super.onPostExecute(s);
        }
    }

    class getVehicleState extends AsyncTask<String, String, String> {

        HttpResponse response;
        BufferedReader reader;
        String jsonResponse;
        JSONObject jsonObject;
        String connected;

        @Override
        protected String doInBackground(String... strings) {
            try {
                Log.e("IP", url + vehicleState);
                response = httpClient.execute(new HttpGet(url + vehicleState));

                reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
                jsonResponse = reader.readLine();

            } catch (IllegalArgumentException e) {
                return "wrongip";
            } catch (NullPointerException e) {
                return "error";
            } catch (MalformedURLException e) {
                return "error";
            } catch (HttpHostConnectException e) {
                e.printStackTrace();
                return "failed";
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {

            if (s != null) {
                if (s.equals("failed")) {
                    connectedStatus.setText("Connection refused");
                    connectedStatus.setBackground(getResources().getDrawable(R.drawable.rectangle_bar_default));
                    armStatus.setText("No Data");
                    modeStatus.setText("No Data");
                    modeStatus.setBackground(getResources().getDrawable(R.drawable.rectangle_bar_default));
                    armStatus.setBackground(getResources().getDrawable(R.drawable.rectangle_bar_default));
                    return;
                }
            }

            try {
                if (jsonResponse == null) {
                    throw (new NullPointerException());
                }
                jsonObject = new JSONObject(jsonResponse);
                connected = jsonObject.getString("connected");
                Log.e("Success", connected);
                if (connected.equals("true")) {
                    connectedStatus.setText("Connected");
                    connectedStatus.setBackground(getResources().getDrawable(R.drawable.rectangle_bar_disarm));
                    vehicleArm = jsonObject.getString("armed");
                    vehicleMode = jsonObject.getString("mode");
                    if (is_vehicleState) {
                        if (vehicleArm.equals("true")) {
                            armStatus.setText("Armed");
                            armStatus.setBackground(getResources().getDrawable(R.drawable.rectangle_bar_arm));
                        } else {
                            armStatus.setText("Disarmed");
                            armStatus.setBackground(getResources().getDrawable(R.drawable.rectangle_bar_disarm));
                        }
                        modeStatus.setText("" + vehicleMode);
                        modeStatus.setBackground(getResources().getDrawable(R.drawable.rectangle_bar_disarm));

                    }
                } else {
                    //Fligth Controller not connected
                    connectedStatus.setText("Disconnected");
                    connectedStatus.setBackground(getResources().getDrawable(R.drawable.rectangle_bar_default));
                    armStatus.setText("No Data");
                    armStatus.setBackground(getResources().getDrawable(R.drawable.rectangle_bar_default));
                    modeStatus.setText("No Data");
                    modeStatus.setBackground(getResources().getDrawable(R.drawable.rectangle_bar_default));

                }
            } catch (NullPointerException e) {
                connectedStatus.setText("Server not found");
                connectedStatus.setBackground(getResources().getDrawable(R.drawable.rectangle_bar_default));
                armStatus.setText("No Data");
                armStatus.setBackground(getResources().getDrawable(R.drawable.rectangle_bar_default));
                modeStatus.setText("No Data");
                modeStatus.setBackground(getResources().getDrawable(R.drawable.rectangle_bar_default));

            } catch (JSONException e) {
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
                e.printStackTrace();
            }
        }
    }

    protected void showInputDialog() {

        // get prompts.xml view
        LayoutInflater layoutInflater = LayoutInflater.from(MainActivity.this);
        View promptView = layoutInflater.inflate(R.layout.dialogue_design, null, true);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MainActivity.this);
        alertDialogBuilder.setView(promptView);

        final EditText editText = (EditText) promptView.findViewById(R.id.edittext);
        // setup a dialog window
        alertDialogBuilder.setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        height = Integer.parseInt(editText.getText().toString().trim());
                        new takeoff().execute(url + takeoff);
                    }
                })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

        // create an alert dialog
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

    class server_powerstatus extends AsyncTask<String, String, String> {
        HttpResponse response;
        BufferedReader reader;
        String jsonResponse;
        JSONObject jsonObject;

        @Override
        protected String doInBackground(String... strings) {
            try {
                response = httpClient.execute(new HttpGet(strings[0]));
                reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
                jsonResponse = reader.readLine();

            } catch (IOException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            try {
                jsonObject = new JSONObject(jsonResponse);
                String VoltageResult = jsonObject.getString("voltage");
                // Log.e(TAG,"Voltagef Result "+jsonObject.getString("voltage"));
                Double voltage = Double.parseDouble(VoltageResult);
                //Log.e(TAG,"Voltagef Result "+voltage);
                double RoundOfVoltage = Math.round(voltage * 100);
                //Log.e(TAG,"Voltagef Result "+RoundOfVoltage);
                if (RoundOfVoltage > 0 && RoundOfVoltage <= 1680) {
                    if (is_powerStatus) {
                        powerstatus_textview.setText((RoundOfVoltage / 100) + "");
                        powerstatus_textview.setBackground(getResources().getDrawable(R.drawable.rectangle_bar_disarm));
                        pb.setProgress((int) RoundOfVoltage);
                    }

                } else {
                    powerstatus_textview.setText("No Data");
                    powerstatus_textview.setBackground(getResources().getDrawable(R.drawable.rectangle_bar_default));
                    pb.setProgress(0);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
            super.onPostExecute(s);
        }
    }

    class server_altitude extends AsyncTask<String, String, String> {
        HttpResponse response;
        BufferedReader reader;
        String jsonResponse;
        JSONObject jsonObject;

        @Override
        protected String doInBackground(String... strings) {
            try {
                response = httpClient.execute(new HttpGet(strings[0]));
                reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
                jsonResponse = reader.readLine();

            } catch (IOException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                Log.e(TAG, "Altitude NullPointer");
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            try {
                jsonObject = new JSONObject(jsonResponse);
                String alitude = jsonObject.getJSONObject("twist").getJSONObject("linear").getString("z");
                Double double_altitude = Double.parseDouble(alitude);
                //Log.e(TAG,"Altitude Result "+double_altitude);
                double RoundOfAltitude = Math.round(double_altitude * 100);
                //Log.e(TAG,"Altitude Result "+RoundOfAltitude);
                // int int_altitude = (Integer.parseInt(alitude)*(-1));
                text_altitude.setAlpha(0.8f);
                if (is_altitude) {
                    text_altitude.setText("" + (RoundOfAltitude / 100) * (-1));
                }
                //Log.e(TAG,"JSonAltitude"+alitude);
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
            super.onPostExecute(s);
        }
    }

    HttpResponse httpResponse;
    DefaultHttpClient httpClient;
    HttpPost httpPost;
    HttpEntity httpEntity;

    class server_velocity extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... strings) {
            Log.e(mydebug, "Call Server Velocity" + url + ros_server_service + string_velocity_set);
            httpClient = new DefaultHttpClient();
            httpPost = new HttpPost(url + ros_server_service + string_velocity_set);
            Log.e(mydebug, "check" + url + ros_server_service + string_velocity_set);
            JSONObject check = velocity.getTwist();
            Log.e(mydebug, "check" + check);
            httpPost.setEntity(new ByteArrayEntity(check.toString().getBytes()));
            try {
                httpResponse = httpClient.execute(httpPost);
                httpEntity = httpResponse.getEntity();
                is = httpEntity.getContent();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            Log.e(mydebug, " Json httpResponse  " + httpResponse);
            Log.e(mydebug, " Json httpEntity " + httpEntity);
            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                    Log.i(mydebug, "Read line :" + line);
                }
                is.close();
                Log.e(mydebug, " Json reader " + reader);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            super.onPostExecute(s);
        }
    }

    class server_hud extends AsyncTask<String, String, String> {
        HttpResponse response;
        BufferedReader reader;
        String jsonResponse;
        JSONObject jsonObject;

        @Override
        protected String doInBackground(String... strings) {
            try {
                // Log.e(TAG,"HUD URl :"+strings[0]);
                response = httpClient.execute(new HttpGet(strings[0]));
                reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
                jsonResponse = reader.readLine();

            } catch (IOException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            try {
                jsonObject = new JSONObject(jsonResponse);
                //Log.e(TAG,"HuD :"+jsonResponse);
                JSONObject orientation = jsonObject.getJSONObject("orientation");

                Double w = orientation.getDouble("w");
                Double x = orientation.getDouble("x");
                Double y = orientation.getDouble("y");
                Double z = orientation.getDouble("z");
                double ysqr = y * y;

                // roll (x-axis rotation)
                double t0 = +2.0 * (w * x + y * z);
                double t1 = +1.0 - 2.0 * (x * x + ysqr);
                Double roll = Math.atan2(t0, t1);
                //Log.e(TAG,"Roll :"+Math.round(roll*100));
                if (is_hud) {
                    drone.setRotation(Math.round(roll * 100));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
            super.onPostExecute(s);
        }

    }
    BufferedReader reader;
    String jsonResponse;
    class head_apps extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Log.e(TAG,"Call Pre For Head");

        }
        @Override
        protected String doInBackground(String... params) {
            httpClient = new DefaultHttpClient();
            httpPost = new HttpPost(url+ros_server_service+exec_script);
            Log.e(TAG,"URL for Head:"+url+ros_server_service+exec_script);
            String check = "{\"app_name\" : \"head_" + params[0] + ".py\",\"arguments\" : \"\" }";
            Log.e(TAG,"Params "+ check);
            httpPost.setEntity(new ByteArrayEntity(check.getBytes()));

            try {
                httpResponse = httpClient.execute(httpPost);
                //httpEntity = httpResponse.getEntity();
                //is = httpEntity.getContent();
                reader = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent(), "UTF-8"));
                jsonResponse = reader.readLine();
                Log.e(TAG,"Head Url Call :"+jsonResponse);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

        }
    }
    class server_distance_data extends AsyncTask<String, String, String> {
        HttpResponse response;
        BufferedReader reader;
        String jsonResponse;
        JSONObject jsonObject;
        @Override
        protected void onPreExecute() {
            //Log.e(TAG,"Pre Distnace call :");
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                // Log.e(TAG,"HUD URl :"+strings[0]);
                response = httpClient.execute(new HttpGet(url+":5000/dist"));
                //Log.e(TAG,"Object Distance 1:"+response+" URL :"+url+":5000/dist");
                httpEntity = response.getEntity();
                is = httpEntity.getContent();

            } catch (IOException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            try {

                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);

                StringBuilder sb = new StringBuilder();
                String line = new String();
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                    //Log.e(TAG,"json Object 1"+line);

                }
                is.close();
                jsonObject = new JSONObject(sb.toString());
                Log.e(TAG,"json Object Dist :"+jsonObject.getString("dist"));
                textview_object_distance.setText(jsonObject.getString("dist"));
                gridView2.setBackground(getResources().getDrawable(R.drawable.rectangle_bar_disarm));

            } catch (JSONException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            super.onPostExecute(s);
        }

    }
    class server_head_data extends AsyncTask<String, String, String> {
        HttpResponse response;
        BufferedReader reader;
        String jsonResponse;
        JSONObject jsonObject;

        @Override
        protected String doInBackground(String... strings) {
            try {
                // Log.e(TAG,"HUD URl :"+strings[0]);
                response = httpClient.execute(new HttpGet(url+":5000/head"));
                httpEntity = response.getEntity();
                is = httpEntity.getContent();

            } catch (IOException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);

                StringBuilder sb = new StringBuilder();
                String line = new String();
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                    //Log.e(TAG,"json Object 1"+line);
                }
                is.close();
                jsonObject = new JSONObject(sb.toString());
                Log.e(TAG,"json Object Head :"+jsonObject.getString("head"));
                textview_display_number_of_head.setText(jsonObject.getString("head"));
                gridView1.setBackground(getResources().getDrawable(R.drawable.rectangle_bar_disarm));

            } catch (JSONException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            super.onPostExecute(s);
        }

    }
}