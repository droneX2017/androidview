package com.example.jagdish.remoteview;
import org.json.JSONException;
import org.json.JSONObject;
/**
 * Created by parth on 24/3/17.
 */
public class Velocity {
    JSONObject twist1 = new JSONObject();
    JSONObject twist2=new JSONObject();
    float vx=0,vy=0,vz=0,tolerance=2;
    double yaw_rate=0;
    Boolean yaw_rate_valid=false,relative=true,async=true,body_frame=true;

    public void setTwist(float vx,float vy, float vz, double yaw_rate, boolean yaw_rate_valid) {
        this.vx = vx;
        this.vy = vy;
        this.vz = vz;
        this.yaw_rate = yaw_rate;
        this.yaw_rate_valid = yaw_rate_valid;
    }
    public JSONObject getTwist(){
        JSONObject linear=new JSONObject();
        JSONObject linear_data=new JSONObject();
        JSONObject angular=new JSONObject();
        twist2=new JSONObject();
        twist1 =new JSONObject();
        try {
            linear_data.put("x",vx);
            linear_data.put("y",vy);
            linear_data.put("z",vz);
            angular.put("z",yaw_rate);
            linear.put("angular",angular);
            linear.put("linear",linear_data);
            twist2.put("twist",linear);
            twist1.put("twist",twist2);
            twist1.put("tolerance", tolerance);
            twist1.put("async",async);
            twist1.put("relative",relative);
            twist1.put("yaw_rate_valid",yaw_rate_valid);
            twist1.put("body_frame",body_frame);
            // Log.e(TAG,"Linear data "+linear_data);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return twist1;
    }

}
