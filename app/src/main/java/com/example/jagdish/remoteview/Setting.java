package com.example.jagdish.remoteview;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.ProgressBar;
import android.widget.Toast;

public class Setting extends AppCompatActivity {

    private View mDecorView;
    private ProgressBar p1, p2;
    private String meunItem[] = {"Path Flow", "Object Flow", "Photography", "Profile"};
    private SwitchCompat switch_altitude,switch_powerStatus,switch_vehicleState,switch_hud,switch_head_script,switch_head_data,switch_distance_data;

    String mydebug="Setting";
    SharedPreferences sharedPreferences ;
    SharedPreferences.Editor editor;
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

        if (Build.VERSION.SDK_INT >= 19 && Build.VERSION.SDK_INT < 21) {
            setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, true);
            setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION, true);
        }
        if (Build.VERSION.SDK_INT >= 21) {
            setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, true);
            setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION, true);
            getWindow().setNavigationBarColor(Color.GRAY);
        }
        sharedPreferences = getSharedPreferences("RemoteSetting", Context.MODE_PRIVATE);


        editor=sharedPreferences.edit();

        switch_altitude=(SwitchCompat) findViewById(R.id.switch_altitude);
        switch_powerStatus=(SwitchCompat) findViewById(R.id.switch_powerStatus);
        switch_vehicleState=(SwitchCompat) findViewById(R.id.switch_vehicleState);
        switch_hud=(SwitchCompat) findViewById(R.id.switch_hud);
        switch_head_script=(SwitchCompat) findViewById(R.id.switch_head_script);
        switch_head_data=(SwitchCompat) findViewById(R.id.switch_head_data);
        switch_distance_data=(SwitchCompat) findViewById(R.id.switch_distance_data);

        switch_altitude.setScaleX((float) 1.5);
        switch_altitude.setScaleY((float) 1.5);

        switch_powerStatus.setScaleX((float) 1.5);
        switch_powerStatus.setScaleY((float) 1.5);

        switch_vehicleState.setScaleX((float) 1.5);
        switch_vehicleState.setScaleY((float) 1.5);

        switch_hud.setScaleX((float) 1.5);
        switch_hud.setScaleY((float) 1.5);

        switch_head_data.setScaleX((float) 1.5);
        switch_head_data.setScaleY((float) 1.5);

        switch_head_script.setScaleX((float) 1.5);
        switch_head_script.setScaleY((float) 1.5);

        switch_distance_data.setScaleX((float) 1.5);
        switch_distance_data.setScaleY((float) 1.5);

        Boolean is_altitude= sharedPreferences.getBoolean("is_altitude",false);
        switch_altitude.setChecked(is_altitude);

        Boolean is_powerStatus= sharedPreferences.getBoolean("is_powerStatus",false);
        switch_powerStatus.setChecked(is_powerStatus);

        Boolean is_vehicleState= sharedPreferences.getBoolean("is_vehicleState",false);
        switch_vehicleState.setChecked(is_vehicleState);

        Boolean is_hud= sharedPreferences.getBoolean("is_hud",false);
        switch_hud.setChecked(is_hud);

        Boolean is_head_script= sharedPreferences.getBoolean("is_head_script",false);
        switch_head_script.setChecked(is_head_script);

        Boolean is_head_data= sharedPreferences.getBoolean("is_head_data",false);
        switch_head_data.setChecked(is_head_data);

        Boolean is_distance_data= sharedPreferences.getBoolean("is_distance_data",false);
        switch_distance_data.setChecked(is_distance_data);


        Log.e(mydebug,"Shared  Pref Bool Head data :"+is_head_data +"Head Script :"+ is_head_script);

       // Boolean is_altitude = Boolean.getBoolean(shared_altitude);

        //switch_altitude.setChecked(is_altitude);
        //Log.e(mydebug,"Shared  Pref Boolean :"+is_altitude);


        switch_altitude.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                Toast.makeText(Setting.this," Is Checked :"+b,Toast.LENGTH_LONG).show();
                editor.putBoolean("is_altitude",b);
                editor.commit();
                Log.e(mydebug,"is_altitude :"+b);

                //Toast.makeText(Setting.this," Is Checked Get frome Share:"+sharedPreferences.getBoolean("is_altitude",false),Toast.LENGTH_LONG).show();

            }
        });

        switch_powerStatus.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                Toast.makeText(Setting.this," Is Checked :"+b,Toast.LENGTH_LONG).show();
                editor.putBoolean("is_powerStatus",b);
                editor.commit();
                Log.e(mydebug,"is_powerStatus :"+b);

            }
        });

        switch_vehicleState.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                Toast.makeText(Setting.this," Is Checked :"+b,Toast.LENGTH_LONG).show();
                editor.putBoolean("is_vehicleState",b);
                editor.commit();
                Log.e(mydebug,"is_vehicleState :"+b);

            }
        });

        switch_hud.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                Toast.makeText(Setting.this," Is Checked :"+b,Toast.LENGTH_LONG).show();
                editor.putBoolean("is_hud",b);
                editor.commit();
                Log.e(mydebug,"is_hud :"+b);

            }
        });

        switch_head_script.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                Toast.makeText(Setting.this," Is Checked :"+b,Toast.LENGTH_LONG).show();
                editor.putBoolean("is_head_script",b);
                editor.commit();
                Log.e(mydebug,"is_head_script :"+b);

            }
        });

        switch_head_data.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                Toast.makeText(Setting.this," Is Checked :"+b,Toast.LENGTH_LONG).show();
                editor.putBoolean("is_head_data",b);
                editor.commit();
                Log.e(mydebug,"is_head_data :"+b);

            }
        });

        switch_distance_data.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                Toast.makeText(Setting.this," Is Checked :"+b,Toast.LENGTH_LONG).show();
                editor.putBoolean("is_distance_data",b);
                editor.commit();
                Log.e(mydebug,"is_distance_data :"+b);

            }
        });


        mDecorView = getWindow().getDecorView();
        mDecorView.setOnSystemUiVisibilityChangeListener
                (new View.OnSystemUiVisibilityChangeListener() {
                    @Override
                    public void onSystemUiVisibilityChange(int visibility) {
                        // Note that system bars will only be "visible" if none of the
                        // LOW_PROFILE, HIDE_NAVIGATION, or FULLSCREEN flags are set.
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                hideSystemUI();
                            }
                        }, 1500);
                    }
                });
        mDecorView.setKeepScreenOn(true);

        hideSystemUI();
    }

    public static void setWindowFlag(Activity activity, final int bits, boolean on) {
        Window win = activity.getWindow();
        WindowManager.LayoutParams winParams = win.getAttributes();
        if (on) {
            winParams.flags |= bits;
        } else {
            winParams.flags &= ~bits;
        }
        win.setAttributes(winParams);
    }


    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        if (hasFocus)
            hideSystemUI();
    }

    // This snippet hides the system bars.
    private void hideSystemUI() {
        // Set the IMMERSIVE flag.
        // Set the content to appear under the system bars so that the content
        // doesn't resize when the system bars hide and show.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            mDecorView.setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                            | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                            | View.SYSTEM_UI_FLAG_IMMERSIVE
            );
        }
    }

    // This snippet shows the system bars. It does this by removing all the flags
    // except for the ones that make the content appear under the system bars.
    private void showSystemUI() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            mDecorView.setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            );
        }
    }
    @Override
    public void onBackPressed() {
        finish();
        startActivity(new Intent(Setting.this,MainActivity.class));
        super.onBackPressed();
    }
}